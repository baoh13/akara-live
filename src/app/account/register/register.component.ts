import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Store } from '@ngrx/store';
import { NgxSpinnerService } from 'ngx-spinner';
import { finalize } from 'rxjs/operators'
import { AuthService } from '../../core/authentication/auth.service';
import { UserRegistration }    from '../../shared/models/user.registration';
import * as fromApp from '../../store/app.reducer'
import * as AuthActions from '../../core/authentication/store/auth.actions'
import { Subscription } from 'rxjs';
import { SegmentService } from 'ngx-segment-analytics';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit, OnDestroy {
  @ViewChild('f', {static: true}) signUpForm: NgForm

  loading = false
  success: boolean;
  error: string;
  storeSub: Subscription;
  types = [1, 2]

  userRegistration: UserRegistration = { 
    firstname: '', 
    surname: '', 
    email: '', 
    password: '', 
    jobTitle: '', 
    organization:'', 
    phoneNumber: '',
    name: '',
    type: 0
  };

  submitted: boolean = false;

  constructor(private authService: AuthService,
              private spinner: NgxSpinnerService,
              private store: Store<fromApp.AppState>,
              private segment: SegmentService) {
}
  ngOnDestroy(): void {
    if (this.storeSub){
      this.storeSub.unsubscribe();
    }
  }

  ngOnInit() {    
    this.segment.track('Loaded Register Page ');
    this.storeSub = this.store.select('auth')
              .subscribe(authState => {
                this.loading = authState.signingUp,
                this.success = authState.signUpSuccess,
                this.error = authState.signUpError
              })
  }

  onSubmit(){
    // console.log('registrationform', this.userRegistration)
    // this.spinner.show()
    this.userRegistration.password = this.userRegistration.email + '@Akara2020'
    this.userRegistration.name = this.userRegistration.firstname

    //TODO userRegistration doesnt save all the data in the db - bug
    this.store.dispatch(new AuthActions.SignUpStart(this.userRegistration))
    // TODO Spinner

    // this.authService
    //     .register(this.userRegistration)
    //     .pipe(finalize(() => {
    //       this.spinner.hide()
    //     }))
    //     .subscribe(res => {
    //       if (res){
    //         this.success = true
    //       }
    //     }, error => {
    //       this.error = error
    //     })
  }
}
