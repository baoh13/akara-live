import { Component, OnInit } from '@angular/core';
import { SegmentService } from 'ngx-segment-analytics';
import { NgxSpinnerService } from 'ngx-spinner';
import { AuthService } from '../../core/authentication/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit { 

  constructor(private authService: AuthService, 
              private spinner: NgxSpinnerService,
              private segment: SegmentService) { }    
  
    title = "Login";
    
    login() {     
      this.spinner.show();
      this.authService.login();
    }   

    ngOnInit() {
      this.segment.track('Loaded Login Page ');
    }
}


 