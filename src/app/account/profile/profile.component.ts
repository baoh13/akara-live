import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Store } from '@ngrx/store';
import { SegmentService } from 'ngx-segment-analytics';
import { NgxSpinnerService } from 'ngx-spinner';
import { map, take, tap } from 'rxjs/operators';
import { AuthService } from 'src/app/core/authentication/auth.service';
import { Profile } from 'src/app/shared/models/profile';
import * as fromApp from '../../store/app.reducer'
import * as ProfileActions from './store/profile.actions'

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  @ViewChild('f', {static: true}) profileForm: NgForm

  profile: Profile = {
    id: '',
    firstName: '',
    surname: '',
    jobTitle: '',
    organization: '',
    email: '',
    phoneNumber: '',
    about: '',
    name: '',
    website: ''
  }

  constructor(private authService: AuthService, 
              private spinner: NgxSpinnerService,
              private store: Store<fromApp.AppState>,
              private segment: SegmentService) { }

  async ngOnInit() {
    this.segment.track('Loaded Profile Page ');
    if (await this.authService.isAuthenticated()){

      this.store.select('profile')
                .pipe(take(1), 
                      map(profileState => { return profileState.profile}),
                      map(profile => {
                        if (!profile){
                          // console.log('No profile', profile)
                          this.store.select('auth')
                          .pipe(take(1), map(authState => {
                            return authState.user
                          }))
                          .subscribe(user => {
                            this.profile = {
                              id: user.profile.sub,
                              about: user.profile.About,
                              email: user.profile.email,
                              firstName: user.profile.name,
                              surname: user.profile.family_name,
                              jobTitle: user.profile.JobTitle,
                              organization: user.profile.Organization,
                              phoneNumber: user.profile.PhoneNumber,
                              website: user.profile.website,
                              name: user.profile.name
                            }
                          })
          
                          this.store.dispatch(new ProfileActions.SetProfile(this.profile))
                          return this.profile                          
                        }
                        else {
                          return profile
                        }
                      }))
                .subscribe(profile => {
                  this.profile = profile
                })
    }
  }

  onSubmit(){
    // console.log('Profile form submitted', this.profileForm.form)
    // this.spinner.show()

    this.segment.track('Update Profile', this.profile);

    this.store.dispatch(new ProfileActions.UpdateProfile(this.profile))

    // this.authService.update(this.profile)
    //                 .pipe(finalize(() => {
    //                   this.spinner.hide()                      
    //                 }))
    //                 .subscribe(res => {
    //                   console.log(res)
    //                 })
  }
}
