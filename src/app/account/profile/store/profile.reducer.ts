import { Profile } from 'src/app/shared/models/profile'
import * as ProfileActions from './profile.actions'

export interface State {
    profile: Profile,
    updatingProfile: boolean,
    updateProfileError: string
}

const initiateState: State = {
    profile: null,
    updatingProfile: false,
    updateProfileError: null
}

export function profileReducer(state = initiateState, action: ProfileActions.ProfileActionTypes){
    switch(action.type){
        case ProfileActions.SET_PROFILE:
            return {
                ...state,
                profile: action.payload,
                updatingProfile: false,
                updateProfileError: null                
            }
        case ProfileActions.UPDATE_PROFILE_SUCCESS:
            return {
                ...state,
                profile: action.payload,
                updatingProfile: false
            }
        case ProfileActions.UPDATE_PROFILE:
            return {
                ...state,
                profile: null,
                updatingProfile: true,
                updateProfileError: null
            }
        case ProfileActions.UPDATE_PROFILE_FAIL:
            return {
                ...state,
                profile: null,
                updateProfileError: action.payload,
                updatingProfile: false
            }
        default:
            return state
    }
}