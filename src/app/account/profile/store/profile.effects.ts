import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, switchMap, tap } from 'rxjs/operators';
import { AuthService } from 'src/app/core/authentication/auth.service';
import * as ProfileActions from './profile.actions'

@Injectable()
export class ProfileEffects {

    constructor(private actions$: Actions, private authService: AuthService){}

    handleError = () => {

    }

    @Effect({dispatch: true})
    profileUpdate = this.actions$.pipe(        
                        ofType(ProfileActions.UPDATE_PROFILE), 
                        switchMap((action: ProfileActions.UpdateProfile) => {
                            return this.authService.update(action.payload)
                                                   .pipe(map((resData) => {
                                                            new ProfileActions.SetProfile(action.payload);
                                                            return new ProfileActions.UpdateProfileSuccess(action.payload)
                                                        }), catchError((errorRes) => {
                                                            const errorMsg = 'Unknown error occured updating profile'
                                                            return of(new ProfileActions.UpdateProfileFail(errorMsg))
                                                        }))
                        }))
}