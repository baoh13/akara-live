import { Action } from '@ngrx/store';
import { Profile } from 'src/app/shared/models/profile';

export const SET_PROFILE = '[PROFILE] SET Profile'
export const UPDATE_PROFILE = '[PROFILE] Update Profile'
export const UPDATE_PROFILE_SUCCESS = '[PROFILE] Update Profile Success'
export const UPDATE_PROFILE_FAIL = '[PROFILE] Update Profile Fail'

export class UpdateProfile implements Action {
    readonly type = UPDATE_PROFILE;

    constructor(public payload: Profile){}
}

export class UpdateProfileSuccess implements Action {
    readonly type = UPDATE_PROFILE_SUCCESS

    constructor(public payload: Profile){}
}

export class UpdateProfileFail implements Action {
    readonly type = UPDATE_PROFILE_FAIL
    constructor(public payload: string){}
}

export class SetProfile implements Action {
    readonly type = SET_PROFILE
    constructor(public payload: Profile){}
}

export type ProfileActionTypes = UpdateProfile | UpdateProfileSuccess | UpdateProfileFail | SetProfile