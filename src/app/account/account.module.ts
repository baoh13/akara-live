import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule }   from '@angular/forms';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { SharedModule }   from '../shared/shared.module';

import { AccountRoutingModule } from './account.routing-module';
import { AuthService }  from '../core/authentication/auth.service';
import { ProfileComponent } from './profile/profile.component';
import { EventOrganizersComponent } from './event-organizers/event-organizers.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { CookiesPolicyComponent } from './cookies-policy/cookies-policy.component';
import { TermsConditionsComponent } from './terms-conditions/terms-conditions.component';

@NgModule({
  declarations: [LoginComponent, RegisterComponent, ProfileComponent, EventOrganizersComponent, PrivacyPolicyComponent, CookiesPolicyComponent, TermsConditionsComponent],
  providers: [AuthService],
  imports: [
    CommonModule,
    FormsModule,
    AccountRoutingModule,
    SharedModule  
  ]
})
export class AccountModule { }
