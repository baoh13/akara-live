import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { Shell } from './../shell/shell.service';
import { ProfileComponent } from './profile/profile.component';
import { AuthGuard } from '../core/authentication/auth.guard';
import { EventOrganizersComponent } from './event-organizers/event-organizers.component';
import { CookiesPolicyComponent } from './cookies-policy/cookies-policy.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { TermsConditionsComponent } from './terms-conditions/terms-conditions.component';

const routes: Routes = [
Shell.childRoutes([
    { path: 'login', component: LoginComponent },
    { path: 'register', component: RegisterComponent },
    { path: 'eventorganizers', component: EventOrganizersComponent },
    { path: 'privacy-policy', component: PrivacyPolicyComponent },
    { path: 'cookies-policy', component: CookiesPolicyComponent },
    { path: 'terms-conditions', component: TermsConditionsComponent },
    { path: 'profile', component: ProfileComponent, canActivate: [AuthGuard] }
  ])
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class AccountRoutingModule { }