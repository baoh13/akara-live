import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReviewFormComponent } from './review-form.component';
import { SharedModule } from '../shared/shared.module';
import { ReviewFormRoutingModule } from './review-form-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FooterModule } from '../shell/footer/footer.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { RatingStarsModule } from '../evaluation-form/rating-stars/rating-stars.module';

@NgModule({
  declarations: [ 
    ReviewFormComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    ReviewFormRoutingModule,
    FooterModule,
    RatingStarsModule
  ]
})
export class ReviewFormModule { }
