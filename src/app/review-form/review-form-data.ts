export const RATINGS = ['', 'Outstanding', 'Excellent', 'Mediocre', 'Poor', 'Terrible', 'N/A']

export const RATING_CRITERIAS = [
  'Quality of speakers & content', 'Networking opportunities', 'Seniority of attendees', 'Quality of business meetings', 'Value for money/time',  
]

export const DELEGATE_TYPES = ['Delegate', 'Speaker', 'Sponsor/Exhibitor', 'Press/Media']

