import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { ResourceApi } from '../core/services/resource-api';
import { Review } from '../shared/models/review';

@Component({
    selector: 'app-review-form-dialog',
    templateUrl: './review-form-dialog-component.html',
    styleUrls: ['./review-form-dialog-component.scss']
})
export class ReviewFormDialogComponent implements OnInit {
    meetingFeature = false;
    review : Review;

    constructor(
        public dialogRef: MatDialogRef<ReviewFormDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private router: Router,
        private resourceApi: ResourceApi) {}

    ngOnInit(): void {
        this.review = this.data.review;
    }

    onClickYes(){
        // console.log('meetingFeature', this.meetingFeature)

        if (this.review 
            && this.meetingFeature
            && this.review.useNewFeatures !== this.meetingFeature){
            this.review.useNewFeatures = this.meetingFeature;
            this.resourceApi.updateReview(this.review).subscribe(res => {
                // console.log('update ', res)
            })
        }

        window.location.href = 'https://www.akaralive.com/'
    }

    onChange(value){
        this.meetingFeature = value;
    }

    onClickNo(){
        window.location.href = 'https://www.akaralive.com/'
    }
}