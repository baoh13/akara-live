import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { finalize } from 'rxjs/operators';
import { ResourceApi } from '../core/services/resource-api';
import { Event } from '../shared/models/event';
import { Review } from '../shared/models/review';
import { DELEGATE_TYPES, RATINGS, RATING_CRITERIAS } from './review-form-data';
import { MatDialog } from '@angular/material/dialog';
import { ReviewFormDialogComponent } from './review-form-dialog.component';
import { ActivatedRoute } from '@angular/router';
import { SegmentService } from 'ngx-segment-analytics';

@Component({
  selector: 'app-review-form',
  templateUrl: './review-form.component.html',
  styleUrls: ['./review-form.component.scss']
})
export class ReviewFormComponent implements OnInit {
  success = false
  ratingHeaders = RATINGS
  ratings;
  ratingCriterias = RATING_CRITERIAS
  delegateTypes = DELEGATE_TYPES
  
  event: Event;
  reviewForm: FormGroup;
  selectedDelegate: '';
  isRecommendedValues = [true, false]
  isRecommended = false;
  eventId = 0;
  isVirtual = false;
  imgUrl: string;

  constructor(
    private resourceApi: ResourceApi, 
    private spinnerService: NgxSpinnerService,
    private dialog: MatDialog,
    private route: ActivatedRoute,
    private segment: SegmentService) {     
  }

  ngOnInit(): void {
    this.segment.track('Loaded Review Form');
    this.ratings = this.ratingHeaders.filter(r => r !== '');

    this.route.queryParams.subscribe(params => {
      if (params['id']){
        switch(params['id']){
          case 'UkFinTechWeek':
            this.eventId = 2
            this.imgUrl = '../../assets/images/IFGS_Logo1.png'
            break;
          case 'IFGS':
            this.eventId = 3
            this.imgUrl = '../../assets/images/IFGS_Logo.png'
            break;
          case 'Pitch360':
            this.eventId = 4
            this.imgUrl = '../../assets/images/Pitch3601.png'
            break
          case 'CityWeek':
            this.eventId = 10
            this.imgUrl = '../../assets/images/CW41.jpg'
            break;
          case 'FSCyberSecuritySummit':
            this.eventId = 12
            this.imgUrl = '../../assets/images/CyberSecurity1.png'
            break
          case 'DataAndFutureOfFinancialServices':
            this.eventId = 13
            this.imgUrl = '../../assets/images/DataAndFutureOfFS1.jpg'            
            break
          case 'AmsterdamFinTechWeek':
            this.eventId = 14
            this.imgUrl = '../../assets/images/XFW_2021_logo1.png'            
            break
          case 'FinTechNorthVirtualLeedsConference':
            this.eventId = 15
            this.imgUrl = '../../assets/images/FinTechNorthLeadsConference.png'            
            break
        case 'The2021PaymentsCanadaSUMMIT':
            this.eventId = 16
            this.imgUrl = '../../assets/images/FinTechNorthLeadsConference.png'            
            break
          case 'OpenBankingWorldCongress':
            this.eventId = 17
            this.imgUrl = '../../assets/images/FinTechNorthLeadsConference.png'            
            break
          default:
            return
        }
      }
    })

    this.spinnerService.show()
    this.resourceApi.getEvents().pipe(finalize(() => {
      this.spinnerService.hide()
    }))
                            .subscribe(events => {
      if (events && events.length > 0){
        this.event = events.find(e => e.id == this.eventId);
        this.isVirtual = this.event.city.toLowerCase() == 'virtual';
      }
    })

    this.reviewForm = new FormGroup({
      overallRating: new FormControl(null, [Validators.required]),
      firstName: new FormControl('', [ Validators.required, Validators.minLength(2)]),
      surname: new FormControl('', []),
      organization: new FormControl('', [ Validators.required, Validators.minLength(2)]),
      email: new FormControl('', [ Validators.required, Validators.email]),
      delegateType: new FormControl('', [ Validators.required ]),
      networkingOpportunities: new FormControl('', []),
      qualityOfSpeakersAndContent: new FormControl('', []),
      qualityOfBusinessMeetings: new FormControl('', []),
      seniorityOfAttendees: new FormControl('', []),
      valueForMoneyAndTime: new FormControl('', []),
      agreedTermsAndConditions: new FormControl(null, [ (control) => {
        return control.value ? null : {'required': true }
      }]),
      isNotCompetitor: new FormControl(true, []),
      recommendToOthers: new FormControl(null, [ Validators.required]),
      reviewHeadline: new FormControl('', [Validators.required]),
      reviewText: new FormControl('', []),
      useFirstName: new FormControl(false, []),
      useNewFeatures: new FormControl(false, []), //1-1 metting
    })
  }

  onSubmit(){
    this.spinnerService.show()

    let review: Review = this.getReview();

    this.segment.track('Add Review', review);

    this.resourceApi.addReview(review)
                    .pipe(finalize(() => {
                      this.spinnerService.hide()
                    }))
                    .subscribe(res => {
      this.reviewForm.reset()
      this.dialog.open(ReviewFormDialogComponent, {
        data: {
          review: res
        },
        disableClose: true
      })
    });
  }

  onSelectOverallRating(value) {
    this.reviewForm.patchValue({
      'overallRating': value
    })
  }

  onSelectDelegate(value) {
    this.selectedDelegate = value
    this.reviewForm.patchValue({
      'delegateType': value
    })
  }

  isActive(delegate){
    return this.selectedDelegate === delegate
  }

  onRatingChanged(event, criteria: string){
    // console.log(criteria, event.value)
    switch(criteria){
      case 'Networking opportunities':
        this.reviewForm.patchValue({
          networkingOpportunities: event.value
        })
        
        break
      case 'Quality of speakers & content':
          this.reviewForm.patchValue({
            qualityOfSpeakersAndContent: event.value
          })
          break
      case 'Value for money/time':
          this.reviewForm.patchValue({
            valueForMoneyAndTime: event.value
          })
          break
      case 'Quality of business meetings':
          this.reviewForm.patchValue({
            qualityOfBusinessMeetings: event.value
          })
          break
      case 'Seniority of attendees':
          this.reviewForm.patchValue({
            seniorityOfAttendees: event.value
          })
          break
      default:
        return
    }
  }

  getReview() {
    let review: Review = {
      id: 0,
      eventId: this.event.id,
      firstName: this.reviewForm.value.firstName,
      lastName: this.reviewForm.value.surname,
      networkingOpportunities: this.reviewForm.value.networkingOpportunities,
      qualityOfBusinessMeetings: this.reviewForm.value.qualityOfBusinessMeetings,
      qualityOfSpeakersAndContent: this.reviewForm.value.qualityOfSpeakersAndContent,
      seniorityOfAttendees: this.reviewForm.value.seniorityOfAttendees,
      valueForMoneyAndTime: this.reviewForm.value.valueForMoneyAndTime,
      agreedTermsAndConditions: this.reviewForm.value.agreedTermsAndConditions,
      ableToContact: this.reviewForm.value.agreedTermsAndConditions,
      isNotCompetitor: this.reviewForm.value.agreedTermsAndConditions,
      delegateType: this.reviewForm.value.delegateType,
      organization: this.reviewForm.value.organization,
      email: this.reviewForm.value.email,
      overallRating: this.reviewForm.value.overallRating,
      recommend: this.reviewForm.value.recommendToOthers,
      useFirstName: this.reviewForm.value.agreedTermsAndConditions, // same as terms and condi
      useNewFeatures: this.reviewForm.value.useNewFeatures,
      reviewHeadline: this.reviewForm.value.reviewHeadline,
      reviewText: this.reviewForm.value.reviewText,
      dateCreated: null
    }

    return review;
  }
}
