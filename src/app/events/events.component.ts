import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Event } from '../shared/models/event';
import * as EventsActions from './store/events.actions';
import * as fromApp from '../store/app.reducer'
import { map } from 'rxjs/operators';
import { ROUTE_ANIMATIONS_ELEMENTS } from '../core/core.module';

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.sass']
})
export class EventsComponent implements OnInit {
  // events: Observable<{events: Eventz[]}>
  loading = false
  events: Event[] = null
  error: string
  
  constructor(private store: Store<fromApp.AppState>) { }

  routeAnimationsElements = ROUTE_ANIMATIONS_ELEMENTS;
   
  ngOnInit() {
    this.store.dispatch(new EventsActions.FetchEventsStart())
    this.store.select('eventsState')
              .pipe(map(eventsState => { return eventsState }))
              .subscribe(eventsState => {
                this.loading = eventsState.loading
                this.events = eventsState.events
                this.error = eventsState.fetchEventError                
                if (this.error){
                  // this.showError(this.error)
                }
              })
    
    
  }

  onUpdate(){
    const updatedEvent = {id: 1, event: new Event(1, 'NewBaoEvent1') }
    this.store.dispatch(new EventsActions.UpdateEvent(updatedEvent))

    // this.recipes[index] = newRecipe
    // this.recipesChanged.next(this.recipes.slices())
  }

  onDelete(index){
    this.store.dispatch(new EventsActions.DeleteEvent(index))

    // this.recipes.splice(index, 1)
    // this.recipesChanged.next(this.recipes.slices())
  }

  // onFetchEvents(){
  //   this.store.dispatch(new EventsActions.FetchEventsStart())
  // }

  onSubmit(){
    let newEvents = [
      new Event(3, 'Baoh3'),
      new Event(4, 'Baoh4')
    ];
    this.store.dispatch(new EventsActions.AddEvents(newEvents))

    // this.recipes.push(recipe)
    // this.recipesChanged.next(this.recipes.slices())
  }
}
