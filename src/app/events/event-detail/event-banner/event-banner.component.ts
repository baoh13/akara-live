import { Component, Input, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { ResourceApi } from 'src/app/core/services/resource-api';
import { Event } from 'src/app/shared/models/event';
import { Review } from 'src/app/shared/models/review';
import { ROUTE_ANIMATIONS_ELEMENTS } from '../../../core/core.module';

@Component({
  selector: 'app-event-banner',
  templateUrl: './event-banner.component.html',
  styleUrls: ['./event-banner.component.scss']
})
export class EventBannerComponent implements OnInit {
  routeAnimationsElements = ROUTE_ANIMATIONS_ELEMENTS;
  @Input() event: Event;
  @Input() reviews: Review[] = [];
  useDarkGrayBackground = false

  overallRating = 0
  totalRating = 0
  numberOfReviews = 0
  starValueSubject: Subject<number> = new Subject<number>();
  imgUrl = ""
  isVirtual = false
  public stars: any[] = new Array(5);
  constructor(private resourceApi: ResourceApi) { }

  ngOnInit(): void {

    if (this.event){
      this.getImg(this.event.id)
      this.overallRating = this.event.overallRating
      this.isVirtual = this.event.city.toLowerCase() == 'virtual'
    }

    this.resourceApi.reviews.subscribe(reviews => {
      if (reviews && reviews.length > 0 && reviews[0].eventId === this.event.id){
        this.numberOfReviews = this.reviews.length
        
        reviews.forEach((review) => {
          if (review.overallRating){
            this.totalRating += review.overallRating
          }
        })
        
        this.numberOfReviews = reviews.length
        this.overallRating = this.totalRating/this.numberOfReviews
        this.overallRating = 0|(this.overallRating + 0.5) 

        this.starValueSubject.next(this.overallRating)
      }
    })
  }

  getImg(eventId){
    switch (eventId){
      case 2:
        this.imgUrl = '../../../../assets/images/UKFW.jpg'
        break
      case 3:
        this.imgUrl = '../../../../assets/images/IFGSpic.jpg'
        break
      case 4:
        this.imgUrl = '../../../../assets/images/Pitch360.jpg'
        break
      case 10:
        this.imgUrl = '../../../assets/images/CW4.jpg'
        break
      case 12:
        this.imgUrl = '../../../assets/images/CyberSecurity.png'
        break
      case 13:
        this.imgUrl = '../../../assets/images/DataAndFutureOfFS.jpg'
        break
      case 14:
        this.imgUrl = '../../../assets/images/XFW_2021_logo.png'
        break
      case 15:
        this.imgUrl = '../../../assets/images/FTN_logo.png'
        break
      case 16:
        this.imgUrl = '../../../assets/images/Summit18-payments-canada.png'
        break
      case 17:
        this.imgUrl = '../../../assets/images/OFW_primary_logo_June20.png'
        break
      default:
        return
    }

  }

}
