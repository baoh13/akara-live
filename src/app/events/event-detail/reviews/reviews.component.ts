import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Store } from '@ngrx/store';
import { Subject } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { ResourceApi } from 'src/app/core/services/resource-api';
import { Review } from 'src/app/shared/models/review';
import * as fromApp from '../../../store/app.reducer';
import { ROUTE_ANIMATIONS_ELEMENTS } from '../../../core/core.module';

export const DATA = [
  { review: {}, star: true },
  { review: {}, star: true },
  { review: {}, star: true },
  { review: {}, star: true },
  { review: {}, star: true },
  { review: {}, star: true },
  { review: {}, star: true },
]

@Component({
  selector: 'app-reviews',
  templateUrl: './reviews.component.html',
  styleUrls: ['./reviews.component.scss']
})
export class ReviewsComponent implements OnInit {
  routeAnimationsElements = ROUTE_ANIMATIONS_ELEMENTS;
  reviews = new Subject<Review[]>();
  @Input() filters$ = new Subject<{key: string, value: number}[]>();

  data = DATA;  
  dataSource = new MatTableDataSource(this.data)
  originalDataSet: Review[] = []
  toShow: Review[] = []

  displayedColumns = ['review']
  isLoadingResults = false

  @Input() eventId;

  _filters: {key: string, value: number}[] = []

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private resourceApi: ResourceApi,
    private store: Store<fromApp.AppState>) { }

  ngOnInit(): void {    
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator
    this.dataSource.sort = this.sort

    this.filters$.subscribe((res : {key: string, value: number}[])  => {
      if (res){
        this._filters = res

        if (this._filters.length == 0) {
          this.dataSource.data = this.originalDataSet.map(r => { return {review: r, star: true} }) 
        }
        else {
          let result: Review[] = [];
          this._filters.forEach(f => {
                              switch(f.key){
                              case 'rating':
                                result = [...result, ...this.originalDataSet.filter(r => r.overallRating === f.value)]
                                break;
                              case 'delegate':
                                result = [...result, ...this.originalDataSet.filter(r => this.getDelegateType(r.delegateType) === f.value)]
                                break;
                              default:
                                return
                            }
                          })
          this.toShow = result
          this.dataSource.data = this.toShow.map(r => { return {review: r, star: true} }) 
        }
      }
    })

    if (this.eventId){
      // this.isLoadingResults = true
      this.resourceApi.getReviewsByEvent(this.eventId)
                      .pipe(tap(res => {
                        this.originalDataSet = res
                      }))
                      .pipe(map(res => {
                        let tableData = res.map(r => { return {review: r, star: true} })                        
                        this.reviews.next(res)
                        return tableData
                      }))
                      .subscribe(tableData => {
                        if (tableData){                          
                          // console.log('tableData', tableData)
                          this.dataSource.data = tableData
                        }
                      })

    }
  }

  // applyFilter(filterValue: string) {
  //   this.dataSource.filter = filterValue.trim().toLowerCase();

  //   if (this.dataSource.paginator) {
  //     this.dataSource.paginator.firstPage();
  //   }
  // }


  getDelegateType(value: string){
    switch(true){
      case value.toLowerCase().includes('delegate'):
        return 4
      case value.toLowerCase().includes('speaker'):
        return 3
      case value.toLowerCase().includes('sponsor'):
        return 2
      case value.toLowerCase().includes('press'):
        return 1
      default:
        return 0
    }
  }
}
