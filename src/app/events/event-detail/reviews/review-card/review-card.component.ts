import { Component, Input, OnInit } from '@angular/core';
import { Review } from 'src/app/shared/models/review';
import { ROUTE_ANIMATIONS_ELEMENTS } from '../../../../core/core.module';

@Component({
  selector: 'app-review-card',
  templateUrl: './review-card.component.html',
  styleUrls: ['./review-card.component.scss']
})
export class ReviewCardComponent implements OnInit {
  routeAnimationsElements = ROUTE_ANIMATIONS_ELEMENTS;
  @Input() reviewData: Review;

  speakersAndContent = { text: 'Speakers & content ', numberOfVotes: 0, totalVotes: 5}
  seniorityAttendees = { text: 'Seniority of attendees', numberOfVotes: 0, totalVotes: 5}
  valueForMmoneyTime = { text: 'Value for money/ time', numberOfVotes: 0, totalVotes: 5}
  networkingOpportunities = { text: 'Networking opportunities', numberOfVotes: 0, totalVotes: 5}
  businessMeetings = { text: 'Quality business meetings', numberOfVotes: 0, totalVotes: 5}
  recommendToOthers = { text: 'Event recommended', numberOfVotes: 0, totalVotes: 1}

  constructor() { }

  ngOnInit(): void {
    if (this.reviewData){   
      this.speakersAndContent.numberOfVotes = this.getRating(this.reviewData.qualityOfSpeakersAndContent)
      this.businessMeetings.numberOfVotes = this.getRating(this.reviewData.qualityOfBusinessMeetings)
      this.networkingOpportunities.numberOfVotes = this.getRating(this.reviewData.networkingOpportunities)
      this.seniorityAttendees.numberOfVotes = this.getRating(this.reviewData.seniorityOfAttendees)      
      this.valueForMmoneyTime.numberOfVotes = this.getRating(this.reviewData.valueForMoneyAndTime)
      this.recommendToOthers.numberOfVotes = this.reviewData.recommend ? 1 : 0
    }    
  }

  private getRating(value: string): number {
    switch(value?.toLowerCase()){
      case 'outstanding':
        return 5;
      case 'excellent':
        return 4;
      case 'fair':
        return 3;
      case 'poor':
        return 2;
      case 'terrible':
        return 1;
      default: 
        return 4
    }
  }

  getFirstChar(value) {
    if (value){
      return value.charAt(0);
    }
    return value;
  }

  // get ratingText(sad: number): string{
  //   return "";
  // }
}


