import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { Subject } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { ResourceApi } from 'src/app/core/services/resource-api';
import { Event } from 'src/app/shared/models/event';
import { Review } from 'src/app/shared/models/review';
import * as fromApp from '../../store/app.reducer';
import * as EventsActions from '../store/events.actions'
import { ROUTE_ANIMATIONS_ELEMENTS } from '../../core/core.module'
import * as EventAboutTexts from './event-detail-about'
import { SegmentService } from 'ngx-segment-analytics';

@Component({
  selector: 'app-event-detail',
  templateUrl: './event-detail.component.html',
  styleUrls: ['./event-detail.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class EventDetailComponent implements OnInit {
  routeAnimationsElements = ROUTE_ANIMATIONS_ELEMENTS;
  aboutText = ""
  eventId: number
  event: Event
  features = []
  topics = []
  industries = []
  
  overallRating = 0
  totalReviews = 0
  isReview = false
  overallRatingText = 'Awesome'

  speakersContent = { text: 'Speakers & content', numberOfVotes: 0, totalVotes: 5}
  networkingOpportunities = { text: 'Networking opportunities', numberOfVotes: 0, totalVotes: 5}
  seniorityAttendees = { text: 'Seniority of attendees', numberOfVotes: 0, totalVotes: 5}
  businessMeeting = { text: 'Quality business meetings', numberOfVotes: 0, totalVotes: 5}
  valueMoneyTime = { text: 'Value for money/ time', numberOfVotes: 0, totalVotes: 5}

  outstanding = { text: 'Outstanding', numberOfVotes: 0, totalVotes: this.totalReviews}
  excellent = { text: 'Excellent', numberOfVotes: 0, totalVotes: this.totalReviews}
  fair = { text: 'Fair', numberOfVotes: 0, totalVotes: this.totalReviews}
  poor = { text: 'Poor', numberOfVotes: 0, totalVotes: this.totalReviews}
  terrible = { text: 'Terrible', numberOfVotes: 0, totalVotes: this.totalReviews}
  recommendToOthers = { text: 'Event recommended', numberOfVotes: 0, totalVotes: this.totalReviews}
  
  numberOfReviews$ = new Subject<{key: string, value: number}[]>()
  numberOfReviews: {key: string, value: number}[] = []

  filters: {key: string, value: number}[] = []

  filters$ = new Subject<{key: string, value: number}[]>()

  constructor(private route: ActivatedRoute, 
              private resourceApi: ResourceApi,
              private store: Store<fromApp.AppState>,
              private segment: SegmentService) { }

  ngOnInit() {

    this.aboutText = EventAboutTexts.UKFW_ABOUT

    this.resourceApi.reviews.subscribe(reviews => {
      let totalRating = 0
      let totalQualitySpeakerContent = 0
      let totalnetworkOpportunities = 0
      let totalSeniorityAttendees = 0
      let totalBusinessMeeting = 0
      let totalValueForMoney = 0

      if (this.event){
        this.segment.track('Loaded Event Page ' + this.event.name, this.event);
      }
      

      if (reviews && reviews.length > 0 && reviews[0].eventId === this.eventId){
        let numberOfReviews = reviews.length
        this.overallRating = reviews[0].overallRating;
        let outstandingVotes = reviews.filter(r => r.overallRating && r.overallRating === 5).length
        let excellentVotes = reviews.filter(r => r.overallRating && r.overallRating === 4).length
        let fairVotes = reviews.filter(r => r.overallRating && r.overallRating === 3).length
        let poorVotes = reviews.filter(r => r.overallRating && r.overallRating === 2).length
        let terribleVotes = reviews.filter(r => r.overallRating === 1).length

        

        this.recommendToOthers.numberOfVotes = reviews.filter(r => r.recommend).length

        reviews.forEach(r => {
          // if (r.overallRating){
          //   totalRating += r.overallRating
          // }
          
          totalQualitySpeakerContent += this.getRating(r.qualityOfSpeakersAndContent)
          totalnetworkOpportunities += this.getRating(r.networkingOpportunities)
          totalSeniorityAttendees += this.getRating(r.seniorityOfAttendees)
          totalValueForMoney += this.getRating(r.valueForMoneyAndTime)
          totalBusinessMeeting += this.getRating(r.qualityOfBusinessMeetings)
        })

        // console.log('totalQualitySpeakerContent', totalQualitySpeakerContent, numberOfReviews)

        this.speakersContent.numberOfVotes = this.setOverallRating(totalQualitySpeakerContent/numberOfReviews)
        this.networkingOpportunities.numberOfVotes = this.setOverallRating(totalnetworkOpportunities/numberOfReviews)
        this.seniorityAttendees.numberOfVotes = this.setOverallRating(totalSeniorityAttendees/numberOfReviews)
        this.valueMoneyTime.numberOfVotes = this.setOverallRating(totalValueForMoney/numberOfReviews)
        this.businessMeeting.numberOfVotes = this.setOverallRating(totalBusinessMeeting/numberOfReviews)

        // this.overallRating = this.setOverallRating(totalRating/numberOfReviews)
        this.overallRating = this.event.overallRating;
        this.getOverallRatingText()
        this.totalReviews = numberOfReviews
        
        this.outstanding.totalVotes = this.totalReviews
        this.outstanding.numberOfVotes = outstandingVotes
        this.terrible.totalVotes = this.totalReviews
        this.terrible.numberOfVotes = terribleVotes
        this.poor.totalVotes = this.totalReviews
        this.poor.numberOfVotes = poorVotes
        this.fair.totalVotes = this.totalReviews
        this.fair.numberOfVotes = fairVotes
        this.excellent.totalVotes = this.totalReviews
        this.excellent.numberOfVotes = excellentVotes

        this.recommendToOthers.totalVotes = this.totalReviews
        
        this.countNbrOfVotes(reviews)                
      }
    })

    this.route.params
        .pipe(
          map(params => { 
            this.store.dispatch(new EventsActions.FetchEventsStart())
            return +params['id'] 
          })
          ,switchMap(id => {
            this.eventId = id;
            return this.store.select('eventsState')
          })
          , map(eventsState => {
            return eventsState.events.find(e => e.id === this.eventId)
          }))          
        .subscribe(event => {
          this.event = event
          this.features = this.event?.features.split(',')
          this.industries = this.event?.industry.split(',')
          this.topics = this.event?.topics.split(',')
          this.setAboutText(this.event)
        })
  }

  onSelectFilters(filters: {key: string, value: number}[]){
    this.filters = filters
    this.filters$.next(this.filters)
  }

  private setOverallRating(value: number): number {
    let result = 0|(value + 0.5) 
    return result
  }

  private getRating(value: string): number {
      switch(value?.toLowerCase()){
        case 'outstanding':
          return 5;
        case 'excellent':
          return 4;
        case 'mediocre':
          return 3;
        case 'poor':
          return 2;
        case 'terrible':
          return 1;
        default: 
          return 4
      }
  }

  private getOverallRatingText(){
    switch(this.overallRating){
      case 5:
        this.overallRatingText = 'Awesome'
        break;
      case 4:
        this.overallRatingText = 'Excellent'
        break;
      case 3:
        this.overallRatingText = 'Fair'
        break;
      case 2:
        this.overallRatingText = 'Poor'
        break;
      case 1:
        this.overallRatingText = 'Terrible'
        break;
      default: 
        return 0
    }
  }

  private countNbrOfVotes(reviews: Review[]) {
    let nbrDelegates = reviews.filter(r => r.delegateType.toLowerCase().includes('delegate')).length
    let nbrSpeakers = reviews.filter(r => r.delegateType.toLowerCase().includes('speaker')).length
    let nbrSponsors = reviews.filter(r => r.delegateType.toLowerCase().includes('sponsor')).length
    let nbrPress = reviews.filter(r => r.delegateType.toLowerCase().includes('press')).length
        
        this.numberOfReviews.push({key: 'outstanding', value: this.outstanding.numberOfVotes})
        this.numberOfReviews.push({key: 'excellent', value: this.excellent.numberOfVotes})
        this.numberOfReviews.push({key: 'fair', value: this.fair.numberOfVotes})
        this.numberOfReviews.push({key: 'poor', value: this.poor.numberOfVotes})
        this.numberOfReviews.push({key: 'terrible', value: this.terrible.numberOfVotes})
        this.numberOfReviews.push({key: 'delegate', value: nbrDelegates})
        this.numberOfReviews.push({key: 'speaker', value: nbrSpeakers})
        this.numberOfReviews.push({key: 'sponsor', value: nbrSponsors})
        this.numberOfReviews.push({key: 'press', value: nbrPress})

        this.numberOfReviews$.next(this.numberOfReviews)
  }

  private setAboutText(event : Event) {
    if (event){
      switch(event.id){
        case 2:
          this.aboutText = EventAboutTexts.UKFW_ABOUT
          break;
        case 3:
          this.aboutText = EventAboutTexts.IFGS
          break;
        case 4:
          this.aboutText = EventAboutTexts.PITCH360
          break;
        case 10:
          this.aboutText = EventAboutTexts.CityWeek
          break;
        case 12:
          this.aboutText = EventAboutTexts.FSCyberSecuritySubmit
          break;
        case 13:
          this.aboutText = EventAboutTexts.DataAndFutureOfFinancialServices
          break;
        case 14:
          this.aboutText = EventAboutTexts.AmsterdamFinTechWeek
          break;
        case 15:
          this.aboutText = EventAboutTexts.FinTechNorthVirtualLeedsConference
          break;
        case 16:
          this.aboutText = EventAboutTexts.The2021PaymentsCanadaSUMMIT
          break;
        case 17:
          this.aboutText = EventAboutTexts.OpenBankingWorldCongress
          break;
        default:
          return
      }
    }
  }

  private roundHalf(num): number
  { 
    let result = (Math.round(num*2)/2).toFixed(1)
    return +result; 
  }
}
