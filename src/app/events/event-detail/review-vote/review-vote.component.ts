import { Input } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ThemePalette } from '@angular/material/core';

@Component({
  selector: 'app-review-vote',
  templateUrl: './review-vote.component.html',
  styleUrls: ['./review-vote.component.scss']
})
export class ReviewVoteComponent implements OnInit {
  color: ThemePalette = 'accent';
  @Input() isReview = true
  @Input() data: 
    { text: string, 
      numberOfVotes: number,
      totalVotes: number
    } = { text: 'Default', numberOfVotes: 0, totalVotes: 100}

  constructor() { }

  ngOnInit(): void {
    // console.log('data', this.data)
  }

}
