import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReviewVoteComponent } from './review-vote.component';

describe('ReviewVoteComponent', () => {
  let component: ReviewVoteComponent;
  let fixture: ComponentFixture<ReviewVoteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReviewVoteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReviewVoteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
