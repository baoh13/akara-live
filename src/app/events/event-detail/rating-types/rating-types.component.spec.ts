import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RatingTypesComponent } from './rating-types.component';

describe('RatingTypesComponent', () => {
  let component: RatingTypesComponent;
  let fixture: ComponentFixture<RatingTypesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RatingTypesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RatingTypesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
