import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-rating-types',
  templateUrl: './rating-types.component.html',
  styleUrls: ['./rating-types.component.scss']
})
export class RatingTypesComponent implements OnInit {
  @Output() selectedFilters = new EventEmitter<{key: string, value: number}[]>()
  @Input() inputData: {key: string, value: number}[]
  @Input() data$ = new Subject<{key: string, value: number}[]>()

  filters: {key: string, value: number}[] = []
  outStandingVotes = 0
  excellentVotes = 0
  fairVotes = 0
  poorVotes = 0
  terribleVotes = 0
  delegateVotes = 0
  speakerVotes = 0
  sponsorVotes = 0
  pressVotes = 0
  
  constructor() { }

  ngOnInit(): void {
    this.data$.subscribe(data => {
      if (data.length > 0){
        this.getNbrOfVotes(data)
      }
    })
  }

  showOptions(event){
    let key = event.source.value
    let isChecked = event.checked

    switch(key){
      case 'overallRating5':
        if (isChecked){
          this.filters.push({key: 'rating', value: 5})
        }
        else {
          this.removeFilter('rating', 5)
        }
        break
      case 'overallRating4':
        if (isChecked){
          this.filters.push({key: 'rating', value: 4})
        }
        else {
          this.removeFilter('rating', 4)
        }
        break
      case 'overallRating3':
        if (isChecked){
          this.filters.push({key: 'rating', value: 3})
        }
        else {
          this.removeFilter('rating', 3)
        }
        break
      case 'overallRating2':
        if (isChecked){
          this.filters.push({key: 'rating', value: 2})
        }
        else {
          this.removeFilter('rating', 2)
        }
        break
      case 'overallRating1':
        if (isChecked){
          this.filters.push({key: 'rating', value: 1})
        }
        else {
          this.removeFilter('rating', 1)
        }
        break
      case 'delegate4':
        if (isChecked){
          this.filters.push({key: 'delegate', value: 4})
        }
        else {
          this.removeFilter('delegate', 4)
        }
        break
      case 'delegate3':
        if (isChecked){
          this.filters.push({key: 'delegate', value: 3})
        }
        else {
          this.removeFilter('delegate', 3)
        }
        break
      case 'delegate2':
        if (isChecked){
          this.filters.push({key: 'delegate', value: 2})
        }
        else {
          this.removeFilter('delegate', 2)
        }
        break
      case 'delegate1':
        if (isChecked){
          this.filters.push({key: 'delegate', value: 1})
        }
        else {
          this.removeFilter('delegate', 1)
        }
        break
      default:
        return
    }
    this.selectedFilters.emit(this.filters)
  }

  removeFilter(type: string, value: number){
    let index = this.filters.findIndex(f => f.key === type && f.value === value)
    if (index > -1){
      this.filters.splice(index, 1)
    }
  }

  private getNbrOfVotes(votes) {
    
    votes.forEach((data: {key: string, value: number }) => {            
      switch(data.key.toLowerCase()){
        case "outstanding":          
          this.outStandingVotes = data.value
          break;
        case "excellent":
          this.excellentVotes = data.value
          break;
        case 'fair':
          this.fairVotes = data.value
          break;
        case 'poor':
          this.poorVotes = data.value
          break;
        case 'terrible':
          this.terribleVotes = data.value
          break;
        case 'delegate':
          this.delegateVotes = data.value
          break;
        case 'speaker':
          this.speakerVotes = data.value
          break;
        case 'sponsor':
          this.sponsorVotes = data.value
          break;
        case 'press':
          this.pressVotes = data.value
          break;
        default: 
          return 
      }
    })
  }
}
