export const UKFW_ABOUT = 
`   <p>
        The most anticipated global FinTech event of the year is back! 
    </p>
    <p>
        Now in its 6th year, UK FinTech Week 2021 will celebrate the achievements of the sector thus far, 
        and showcase how the industry empowers resilience and thrives as a sector despite these 
        turbulent times. 
    </p>
    <p>
        FinTech is revolutionising financial services and changing the way we live our lives every single day, 
        especially during this time of global crisis where FinTech app adoption is on the rise.

    </p>
    <p>
        Leveraging the power of technology, agile ways of working and a disruptive and innovative mindset, 
        FinTech is providing consumers and businesses access to better, fairer and more sustainable 
        financial products and services.    
    </p>
`

export const IFGS =  
`   <p>
        IFGS 2021 will bring together delegates from around the world to the UK, 
        the premier global FinTech hub, to see the very best the sector has to offer. 
        The Global Summit will welcome industry leaders ranging from innovators, 
        institutions, regulators to policy-makers, startups and investors. 

    </p>
    <p>
        IFGS 2021 will be a truly global FinTech event, highlighting and celebrating 
        the achievements made around the world in FinTech. Delegates from over 70 countries 
        are set to attend bringing their insights into how FinTech is developing in markets near and far.      
    </p>
`

export const PITCH360 =  
`   <p>24 companies, 8 categories, 3 minutes to pitch.</p>
    <p> 
        Innovate Finance's flagship pitching event is back. 
    </p>
    <p>    
        Pitch360 will welcome 24 shortlisted companies to battle it out on a global stage 
        to pitch their product and business in just 3 minutes. Who wins? The audience decides.
    </p>
`

export const CityWeek =  
`   <p>City Week is the premier gathering of the international financial services community. </p>
    <p> 
        Organised under a partnership between the UK Government's Department for International Trade, the City of London Corporation, 
        TheCityUK, UK Finance, London & Partners and City & Financial Global. 
    </p>
    <p>    
        City Week brings together industry leaders and policy makers 
        from around the globe to consider together the future of global financial markets and London in particular.
    </p>
`

export const FSCyberSecuritySubmit =  
`   <p>Now in its 6th year, the Financial Services Cyber Security Summit, which is regularly supported by the City authorities, 
        is the premier event of its kind.</p>
    <p> 
        Who attends?
    </p>
    <p>    
        Government officials, regulators, academia and senior management of all financial institutions, including chief executives, 
        chief information security officers, chief information officers, chief digital officers, chief risk officers, 
        heads of cyber security, heads of cyber defence, heads of compliance, heads of business continuity, 
        heads of cyber threat intelligence, heads of operational risk, heads of legal, as well as the advisory community across the sector.
    </p>
`
export const DataAndFutureOfFinancialServices =  
`   <p>Managing large volumes of data has become an increasingly important part of doing business in the financial services industry. 
        The increased use of sophisticated technology for data analysis and collection 
        requires businesses, policymakers and regulators to keep pace with new 
        techniques and alternative forms of data. Building a strong institutional 
        culture around data governance and the responsible, transparent and ethical 
        use of machine learning and AI have all become priorities for the UK financial 
        services sector.
    </p>
    <p> 
    To assist companies with the above objectives, and to offer a 
    platform on which industry can discuss its ideas and concerns with government, 
    City & Financial Global are organising the second annual ‘The Future of Data 
    in Financial Services Forum’
    </p>
    <p>    
    The summit will bring together leading experts to provide a cross-cutting view 
    of the emerging regulatory and ethical standards for data analytics, 
    processing and management; to examine the data-related developments 
    of most concern to regulators; and to discuss the fundamental changes 
    that lie ahead. The event will also enable c-suite executives to benefit 
    from real life case studies and best practices in the use, collection and sharing of data.
    </p>
`

export const AmsterdamFinTechWeek =  
`   <p>Holland FinTech has initiated Amsterdam FinTech Week (XFW) since 2018, 
        to further connect the Amsterdam, Dutch and European Fintech ecosystem. 
        This event now takes place each year, to provide a week program full of 
        fintech focused insights, innovations, new connections and new initiatives!
    </p>
`

export const FinTechNorthVirtualLeedsConference =  
`   <p>Following the success of our Virtual Manchester Conference on the 12th 
        November 2020, FinTech North is delighted to host our first Virtual Leeds Conference
         on the 25th February 2021. </p>

    <p>In light of the current situation, the event will be hosted virtually 
    in a webinar format, taking place as a half-day event starting at 10:00 
    on 25th February. </p>
    
    <p>Last year’s Leeds conference attracted over 350 registrations but the event 
    was neither streamed nor recorded, so attendees had to physically be in Leeds. 
    This year’s event will be able to reach a wider audience and will also be 
    recorded for people who register but cannot attend on the event date. </p>

    <p>Ron Kalifa, OBE will cover the independent FinTech Strategic Review 
    which aims to establish priority areas for industry, policy makers, 
    and regulators to explore in order to support the ongoing success 
    of the UK FinTech sector.</p>

    <p>Eve Roodhouse will speak about the Leeds City Region FinTech ecosystem 
    and will update our community on some of Leeds City Council’s fantastic 
    work in FinTech, Tech and beyond.</p>

    <p>Paul Stoddart (President New Payment Platforms, Mastercard) will talk 
    about the Future of Payments, covering topics such as digital ID, 
    open banking and the role of blockchain.</p>

    <p>We’re looking forward to welcoming a range of locally based FinTechs, 
    including several who are new to FinTech North, as part of our Leeds FinTech Showcase, 
    pitching their proposition and presenting innovations to our community. </p>
`

export const The2021PaymentsCanadaSUMMIT =  
`   <p>Holland FinTech has initiated Amsterdam FinTech Week (XFW) since 2018, 
        to further connect the Amsterdam, Dutch and European Fintech ecosystem. 
        This event now takes place each year, to provide a week program full of 
        fintech focused insights, innovations, new connections and new initiatives!
    </p>
`

export const OpenBankingWorldCongress =  
`   <p>Holland FinTech has initiated Amsterdam FinTech Week (XFW) since 2018, 
        to further connect the Amsterdam, Dutch and European Fintech ecosystem. 
        This event now takes place each year, to provide a week program full of 
        fintech focused insights, innovations, new connections and new initiatives!
    </p>
`
               
