import { Component, Input, OnInit } from '@angular/core';
import { Event } from 'src/app/shared/models/event';

@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.sass']
})
export class EventComponent implements OnInit {
  // event: Eventz
  eventId: number

  @Input() event: Event
  @Input() index: number


  constructor() {    
  }

  ngOnInit() {
  }

}
