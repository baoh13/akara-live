import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppAuthGuard } from '../core/authentication/app.auth.guard';
import { AuthGuard } from '../core/authentication/auth.guard';
import { Shell } from '../shell/shell.service';
import { EventDetailComponent } from './event-detail/event-detail.component';
import { EventsComponent } from './events.component';
import { EventsResolverService } from './events.resolver.service';

const routes: Routes = [
    Shell.childRoutes([
        // { path: 'events', component: EventsComponent, canActivate: [AuthGuard]}
        { path: 'events', component: EventsComponent },
        { path: 'events/:id', component: EventDetailComponent }
    ])
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    providers: []
})
export class EventsRoutingModule {}