import { Action } from '@ngrx/store';
import { Event } from 'src/app/shared/models/event';

export const ADD_EVENT = '[Events] Add a new event'
export const FETCH_EVENTS = '[Events] Fetch All Events'
export const FETCH_EVENTS_START = '[Events] Initiate Fetching All Events'
export const FETCH_EVENTS_FAIL = '[Events] Fetching All Events failed'
export const ADD_EVENTS = '[Events] Add events'
export const UPDATE_EVENT = '[Events] Update an event'
export const DELETE_EVENT = '[Events] Delete an event'

export class AddEvent implements Action {
    readonly type = ADD_EVENT;
    constructor(public payload: Event) {}
}

export class AddEvents implements Action {
    readonly type = ADD_EVENTS
    
    constructor(public payload: Event[]) {}
}

export class UpdateEvent implements Action {
    readonly type = UPDATE_EVENT

    constructor(public payload: {id: number, event: Event}){}
}

export class DeleteEvent implements Action {
    readonly type = DELETE_EVENT

    constructor(public payload: number){}
}

export class FetchEvents implements Action {
    readonly type = FETCH_EVENTS

    constructor(public payload: Event[]) {}
}

export class FetchEventsStart implements Action {
    readonly type = FETCH_EVENTS_START
}

export class FetchEventsFail implements Action {
    readonly type = FETCH_EVENTS_FAIL

    constructor(public payload: string){}
}

export type EventsActionTypes = AddEvent 
                                | AddEvents 
                                | UpdateEvent 
                                | DeleteEvent 
                                | FetchEvents 
                                | FetchEventsStart
                                | FetchEventsFail