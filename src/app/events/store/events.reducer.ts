import { Event } from '../../shared/models/event';
import * as EventsActions from './events.actions' 

export interface State {
    events: Event[],
    editedEventIndex: number,
    fetchEventError: string,
    loading: boolean
}

const initialState: State = {
    events: [],
    editedEventIndex: -1,
    fetchEventError: null,
    loading: false
}

export function EventsReducer(state = initialState, action: EventsActions.EventsActionTypes){
    switch(action.type){
        case EventsActions.FETCH_EVENTS_START:
            return {
                ...state,
                fetchEventError: null,
                events: [...state.events],
                loading: true
            }
        case EventsActions.FETCH_EVENTS:
            return {
                ...state,
                events: [...action.payload],
                loading: false
            }
        case EventsActions.FETCH_EVENTS_FAIL:
            return {
                ...state,
                events: [...state.events],
                fetchEventError: action.payload,
                loading: false
            }
        case EventsActions.ADD_EVENT:
            return {
                ...state,
                events: [...state.events, action.payload]
            }
        case EventsActions.ADD_EVENTS:
            return {
                ...state,
                events: [...state.events, ...action.payload]
            }
        case EventsActions.UPDATE_EVENT:
            const event = state.events.find(e => e.id == action.payload.id)
            const index = state.events.findIndex(e => e.id == action.payload.id)

            // override all updated properties 
            const updatedEvent = {
                ...event,
                ...action.payload.event
            }

            let updatedEvents = [...state.events]
            updatedEvents[index] = updatedEvent
            
            return {
                ...state,
                events: updatedEvents
            }
        case EventsActions.DELETE_EVENT:
            return {
                ...state,
                //Return a new list
                events: state.events.filter((e, index) => {
                    return index !== action.payload
                })
            }
        default:
            return state
    }
}