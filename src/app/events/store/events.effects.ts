import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { map, switchMap } from 'rxjs/operators';
import { ResourceApi } from 'src/app/core/services/resource-api';
import { Event } from 'src/app/shared/models/event';
import * as EventsActions from './events.actions';

@Injectable()
export class EventsEffects {
    @Effect()
    fetchEvents = this.actions$.pipe(
        ofType(EventsActions.FETCH_EVENTS_START), 
        // withLatestFrom(this.store.select('eventsState')),
        // switchMap(([actionData, eventState]) => {
            // const events = eventsState.events; // Store events if needed
        switchMap(() => {
            return this.resourceApi.getEvents()
        })

        // Transform the events response
        , map((events: Event[]) => {
            return events
        })
        ,map((events) => {
            return new EventsActions.FetchEvents(events)
        }) 
    )

    constructor(private actions$: Actions,
                private resourceApi: ResourceApi){

    }
}