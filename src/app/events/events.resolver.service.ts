import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Actions, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { map, switchMap, take } from 'rxjs/operators';
import { Event } from '../shared/models/event';
import * as fromApp from '../store/app.reducer';
import * as EventsActions from './store/events.actions';

@Injectable({
    providedIn: 'root'
})
export class EventsResolverService implements Resolve<Event[]> {

    constructor(private store: Store<fromApp.AppState>, private actions$: Actions){}

    resolve(route: ActivatedRouteSnapshot, 
            state: RouterStateSnapshot)
            : Event[] | Observable<Event[]> | Promise<Event[]> {
        
        return this.store.select('eventsState')
                         .pipe(take(1),
                               map(eventState => { return eventState.events}),
                               switchMap(events => {
                                   if (events.length == 0){
                                    // this.store.dispatch(new EventsActions.FetchEventsStart())
                                    return this.actions$.pipe(ofType(EventsActions.FETCH_EVENTS))
                                   }
                                   else {
                                       return of(events)
                                   }
                               }))
    }

}