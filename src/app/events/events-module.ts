import { NgModule } from '@angular/core';
import { EventsComponent } from './events.component';
import { EventsRoutingModule } from './events-routing.module';
import { CoreModule } from '../core/core.module';
import { CommonModule } from '@angular/common';
import { EventComponent } from './event/event.component';
import { EventDetailComponent } from './event-detail/event-detail.component';
import { SharedModule } from '../shared/shared.module';
import { EventBannerComponent } from './event-detail/event-banner/event-banner.component';
import { ReviewVoteComponent } from './event-detail/review-vote/review-vote.component';
import { RatingTypesComponent } from './event-detail/rating-types/rating-types.component';
import { ReviewsComponent } from './event-detail/reviews/reviews.component';
import { ReviewCardComponent } from './event-detail/reviews/review-card/review-card.component';

@NgModule({
    declarations: [ 
        EventsComponent, 
        EventComponent,
        EventDetailComponent,
        EventBannerComponent,
        ReviewVoteComponent,
        RatingTypesComponent,
        ReviewsComponent,
        ReviewCardComponent
    ],
    imports: [
        EventsRoutingModule,
        CoreModule,
        CommonModule,
        SharedModule
    ],
    exports: [],
    providers: []
})
export class EventsModule {}