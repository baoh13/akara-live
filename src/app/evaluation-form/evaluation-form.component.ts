import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { ResourceApi } from '../core/services/resource-api';
import { faCoffee} from '@fortawesome/free-solid-svg-icons'
import { FormControl, NgForm, Validators } from '@angular/forms';
import { Review } from '../shared/models/review';
import { Event } from '../shared/models/event'

@Component({
  selector: 'app-evaluation-form',
  templateUrl: './evaluation-form.component.html',
  styleUrls: ['./evaluation-form.component.scss']
})
export class EvaluationFormComponent implements OnInit, OnDestroy {
  @ViewChild('f', {static: false}) signupForm: NgForm
  eventId: any
  event: Event
  events: Event[]
  subscription : Subscription
  profile = { id: 1, name: 'Innovate Finance'} // TODO hook this to organization hosted the event
  faCoffee = faCoffee
  reviewText=''
  selectedDelegate :any;
  delegateTypes = ['Delegate', 'Speaker', 'Sponsor/Exhibition', 'Press']

  ratingCriterias = [
    'Quality of speakers & content', 'Seniority of attendees', 'Value for money/time',
    'Networking opportunities', 'Quality of business meetings',  
  ]

  ratings = ['', 'Outstanding', 'Excellent', 'Mediocre', 'Poor', 'Terrible']

  recommendEvent = [true, false]
  selectedUseNewFeatures: any
  overallRating: number

  constructor(private route: ActivatedRoute, private resourceApi: ResourceApi) {}
  
  ngOnDestroy(): void {
    this.subscription.unsubscribe()
  }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.eventId = params['eventid']
    })

    this.subscription = this.resourceApi.events$.subscribe(events => {
      this.events = events
      if (this.events.length > 0) {
        this.event = this.events[0]
        // console.log('event', this.event)
      }
      // console.log('event subscribed', events)
    })
  }

  // onSubmit(form: NgForm){
  //   console.log('Form submitted', form)
  // }

  onSubmit(){
    // console.log('form submitted', this.signupForm.form.value)

    let review: Review = {
      id: 0,
      eventId: 0,
      firstName: this.signupForm.form.get('firstName').value,
      lastName: this.signupForm.form.get('lastName').value,
      networkingOpportunities: this.signupForm.form.get('Networking opportunities').value,
      qualityOfBusinessMeetings: this.signupForm.form.get('Quality of business meetings').value,
      qualityOfSpeakersAndContent: this.signupForm.form.get('Quality of speakers & content').value,
      seniorityOfAttendees: this.signupForm.form.get('Seniority of attendees').value,
      valueForMoneyAndTime: this.signupForm.form.get('Value for money/time').value,
      ableToContact: this.signupForm.form.get('ableToContact').value,
      agreedTermsAndConditions: this.signupForm.form.get('ableToContact').value,
      isNotCompetitor: this.signupForm.form.get('isNotCompetitor').value,
      delegateType: this.signupForm.form.get('delegateType').value,
      organization: this.signupForm.form.get('organization').value,
      email: this.signupForm.form.get('email').value,
      overallRating: this.signupForm.form.get('overallRating').value,
      recommend: this.signupForm.form.get('recommend').value,
      useFirstName: this.signupForm.form.get('useFirstName').value,
      useNewFeatures: this.signupForm.form.get('useNewFeatures').value,
      reviewHeadline: this.signupForm.form.get('reviewHeadline').value,
      reviewText: this.signupForm.form.get('reviewText').value,
      dateCreated: null
    }

    // console.log('review obj', review)

    this.resourceApi.addReview(review).subscribe(res => {

    }, error => {

    });

    this.signupForm.reset()
  }

  onSelectDelegate(delegate){
    if (!this.signupForm.form.contains('delegateType')){
      this.signupForm.form.addControl('delegateType', new FormControl(delegate))
    }
    
    this.selectedDelegate = delegate;    
    this.signupForm.form.patchValue({
      'delegateType': delegate
    })
    // console.log('select delegate', this.signupForm.form.value)
  }

  onSelectUseNewFeature(value){
    this.selectedUseNewFeatures = value;

    if (!this.signupForm.form.contains('useNewFeatures')){
      this.signupForm.form.addControl('useNewFeatures', new FormControl(value))
    }

    this.signupForm.form.patchValue({
      'useNewFeatures': (value === 'YES') ? true : false
    })
    
    // console.log('select new features', this.signupForm.form.value)
  }

  isActive(delegate){
    return this.selectedDelegate === delegate
  }

  isActiveUseNewFeatures(value){
    return this.selectedUseNewFeatures === value
  }

  onSelectOverallRating(value){
    this.overallRating = value
    if (!this.signupForm.form.contains('overallRating')){
      this.signupForm.form.addControl('overallRating', new FormControl(this.overallRating))
    }
  }
}
