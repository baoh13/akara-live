import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { EvaluationFormComponent } from './evaluation-form.component';
import { EvaluationFormRoutingModule } from './evaluation-form-routing.module';
import { SharedModule } from '../shared/shared.module';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { FormsModule } from '@angular/forms';

@NgModule({
    imports: [ 
        CommonModule,
        FormsModule,
        RouterModule,
        EvaluationFormRoutingModule,
        SharedModule,
        FontAwesomeModule
    ],
    declarations: [ EvaluationFormComponent ],
    providers: []
})
export class EvaluationFormModule {}