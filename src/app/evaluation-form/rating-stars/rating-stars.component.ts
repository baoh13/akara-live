import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-rating-stars',
  templateUrl: './rating-stars.component.html',
  styleUrls: ['rating-stars.component.scss']
})
export class RatingStarsComponent implements OnInit {
  stars: number[] = [1, 2, 3, 4, 5]
  selectedValue = 0
  @Output() overallRatingSelected = new EventEmitter<number>();

  constructor() { }

  ngOnInit() {
  }

  countStar(value){
    this.selectedValue = value
    this.overallRatingSelected.emit(value)
  }

  addClass(star){

    let ab = ''
    for(let i = 0; i < star; i++){
      ab = 'starId'+i
      document.getElementById(ab).classList.add('selected')
    }
  }

  removeClass(star){
    let ab = ''

    for (let i = star-1; i >= this.selectedValue; i--){
      ab='starId'+i
      document.getElementById(ab).classList.remove('selected')
    }
  }
}
