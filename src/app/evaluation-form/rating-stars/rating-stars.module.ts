import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedModule } from 'src/app/shared/shared.module';
import { RatingStarsComponent } from './rating-stars.component';

@NgModule({
    imports: [
        CommonModule,
        SharedModule
    ],
    exports: [RatingStarsComponent],
    declarations: [RatingStarsComponent]
})
export class RatingStarsModule {}