import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Shell } from '../shell/shell.service';
import { EvaluationFormComponent } from './evaluation-form.component';

const routes: Routes = [
    Shell.childRoutes([
        { path: 'evaluation-form', component: EvaluationFormComponent }
    ])
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    providers: []
})
export class EvaluationFormRoutingModule {

}