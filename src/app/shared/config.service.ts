import { Injectable } from '@angular/core';
 
@Injectable()
export class ConfigService {    

    constructor() {}

    get authApiURI() {
        // return 'https://localhost:32794/api';
        return 'https://akara-auth-server.azurewebsites.net/api';
    }    
     
    get resourceApiURI() {
        return 'https://akara-resource-api.azurewebsites.net/api';
    }  
}