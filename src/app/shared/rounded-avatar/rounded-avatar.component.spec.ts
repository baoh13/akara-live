import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RoundedAvatarComponent } from './rounded-avatar.component';

describe('RoundedAvatarComponent', () => {
  let component: RoundedAvatarComponent;
  let fixture: ComponentFixture<RoundedAvatarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoundedAvatarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoundedAvatarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
