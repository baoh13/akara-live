import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-rounded-avatar',
  templateUrl: './rounded-avatar.component.html',
  styleUrls: ['./rounded-avatar.component.scss']
})
export class RoundedAvatarComponent implements OnInit {
  @Input() imgUrl: string;
  @Input() isReviewForm: boolean;
  
  constructor() { }

  ngOnInit(): void {
    if (this.isReviewForm){
      this.imgUrl = '../../../assets/images/avatar.png'
    }
  }

}
