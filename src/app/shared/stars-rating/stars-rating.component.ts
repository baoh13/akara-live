import { Component, Input, OnInit, SimpleChanges } from '@angular/core';
import { Subject } from 'rxjs';

const MAX_NUMBER_OF_STARS = 5

@Component({
  selector: 'app-stars-rating',
  templateUrl: './stars-rating.component.html',
  styleUrls: ['./stars-rating.component.scss']
})
export class StarsRatingComponent implements OnInit {
  public stars: any[] = new Array(5);
  @Input() starValueSubject$: Subject<number> = new Subject<number>();
  @Input() starValue: number = 0;
  @Input() useDarkGrayBackground = true;


  constructor() { }

  ngOnInit(): void { 
  }

  checked(i){
    return this.starValue >= i;
  }
  
  ngOnChanges() {}
}
