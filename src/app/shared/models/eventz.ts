export class Eventz {
    constructor(public id: number, public name: string) {        
    }

    date: Date;
    address: string;
    city: string;
    country: string;
    about: string;
    numberOfAttendees: number;
    features: string;
    industry: string;
    topics: string;
    email: string;
    website: string;
    images: string;
    imageUrl: string;
}