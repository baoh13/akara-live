export interface Profile {
    id: string;
    name: string;
    firstName: string;
    surname: string;
    about: string;
    jobTitle: string;
    organization: string;
    phoneNumber: string;
    email: string;
    website: string;
}