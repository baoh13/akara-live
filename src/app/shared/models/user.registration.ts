export class UserRegistration {

    constructor(
        public firstname: string,
        public surname: string,
        public email: string,
        public password: string,
        public organization: string,
        public jobTitle: string,
        public phoneNumber: string,
        public name: string, 
        public type: number
      ) {  }
}
