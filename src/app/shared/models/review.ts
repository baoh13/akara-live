export interface Review {
    id: number;
    firstName: string;
    lastName: string;
    networkingOpportunities: string;
    qualityOfBusinessMeetings: string;
    qualityOfSpeakersAndContent: string;
    seniorityOfAttendees: string;
    valueForMoneyAndTime: string;
    ableToContact: boolean;
    isNotCompetitor: boolean;
    delegateType: string;
    organization: string;
    email: string;
    overallRating: number;
    recommend: boolean;
    useFirstName: boolean;
    useNewFeatures: boolean;
    reviewHeadline: string;
    reviewText: string;
    agreedTermsAndConditions: boolean;
    eventId: number,
    dateCreated: Date
}