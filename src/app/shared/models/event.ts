// export interface Event {
//     id: number;
//     name: string;
//     date: Date;
//     address: string;
//     city: string;
//     country: string;
//     about: string;
//     numberOfAttendees: number;
//     features: string;
//     industry: string;
//     topics: string;
//     email: string;
//     website: string;
//     images: string;
// }

export class Event {
    constructor(public id: number, public name: string){}

    date: Date;
    address: string;
    city: string;
    country: string;
    about: string;
    numberOfAttendees: number;
    features: string;
    industry: string;
    topics: string;
    email: string;
    website: string;
    images: string;
    imageUrl: string;
    featuredDate: Date;
    organizer: string;
    overallRating: number;
}