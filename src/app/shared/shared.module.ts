// include directives/components commonly used in features modules in this shared modules
// and import me into the feature module
// importing them individually results in: Type xxx is part of the declarations of 2 modules: ... Please consider moving to a higher module...
// https://github.com/angular/angular/issues/10646  

import { NgModule }           from '@angular/core';
import { CommonModule }       from '@angular/common';
 
import { NgxSpinnerModule } from 'ngx-spinner';
import { AutofocusDirective } from './directives/auto-focus.directive';
import { MaterialModule } from '../material.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FlexLayoutModule } from '@angular/flex-layout';
import { StarsRatingComponent } from './stars-rating/stars-rating.component';
import { RoundedAvatarComponent } from './rounded-avatar/rounded-avatar.component';

//https://stackoverflow.com/questions/41433766/directive-doesnt-work-in-a-sub-module
//https://stackoverflow.com/questions/45032043/uncaught-error-unexpected-module-formsmodule-declared-by-the-module-appmodul/45032201

@NgModule({
  imports:      [
    CommonModule, 
    NgxSpinnerModule, 
    MaterialModule,
    BrowserAnimationsModule,
    FlexLayoutModule
  ],
  declarations: [AutofocusDirective, StarsRatingComponent, RoundedAvatarComponent],
  exports:      [
    NgxSpinnerModule, 
    AutofocusDirective, 
    MaterialModule,
    FlexLayoutModule,
    StarsRatingComponent,
    RoundedAvatarComponent
  ],
  providers:    []
})
export class SharedModule { }