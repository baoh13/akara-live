import { Component, Input, OnInit } from '@angular/core';
import { Event } from 'src/app/shared/models/event';
import { ROUTE_ANIMATIONS_ELEMENTS } from 'src/app/core/core.module';

@Component({
  selector: 'app-event-card',
  templateUrl: './event-card.component.html',
  styleUrls: ['./event-card.component.scss']
})
export class EventCardComponent implements OnInit {
  routeAnimationsElements = ROUTE_ANIMATIONS_ELEMENTS;
  starValue = 0;
  isVirtual = false;
  path = "events/"
  @Input() imgUrl: string;
  @Input() event: Event;
  constructor() { }

  ngOnInit(): void {

    if (this.event){
      this.path = "../events/" + this.event.id

      this.isVirtual = this.event.city.toLowerCase() == 'virtual';
    }

    switch (this.event.id){
      case 2:
        this.imgUrl = '../../../assets/images/UKFW.jpg'
        this.starValue = this.event.overallRating
        break
      case 3:
        this.imgUrl = '../../../assets/images/IFGSpic.jpg'
        this.starValue = this.event.overallRating
        break
      case 4:
        this.imgUrl = '../../../assets/images/Pitch360.jpg'
        this.starValue = this.event.overallRating
        break
      case 10:
        this.imgUrl = '../../../assets/images/CW4.jpg'
        this.starValue = this.event.overallRating
        break
      case 12:
        this.imgUrl = '../../../assets/images/CyberSecurity.png'
        this.starValue = this.event.overallRating
        break
      case 13:
        this.imgUrl = '../../../assets/images/DataAndFutureOfFS.jpg'
        this.starValue = this.event.overallRating
        break
      case 14:
        this.imgUrl = '../../../assets/images/XFW_2021_logo.png'
        this.starValue = this.event.overallRating
        break
      case 15:
        this.imgUrl = '../../../assets/images/NCircle.png'
        this.starValue = this.event.overallRating
        break
      case 16:
        this.imgUrl = '../../../assets/images/Summit18-payments-canada.png'
        this.starValue = this.event.overallRating
        break
      case 17:
        this.imgUrl = '../../../assets/images/OFW_primary_logo_June20.png'
        this.starValue = this.event.overallRating
        break
      default:
        return
    }
  }
}
