import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common'; 
import { RouterModule } from '@angular/router';
import { IndexComponent } from './index/index.component';
import { HomeRoutingModule } from './home-routing.module';
import { SharedModule } from '../shared/shared.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { EventCardComponent } from './event-card/event-card.component';
import { RatingStarsModule } from '../evaluation-form/rating-stars/rating-stars.module';

@NgModule({
  declarations: [IndexComponent, EventCardComponent],
  imports: [
    CommonModule,
    RouterModule,
    HomeRoutingModule,
    SharedModule,
    FlexLayoutModule,
    RatingStarsModule
  ]
})
export class HomeModule { }
