import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { ROUTE_ANIMATIONS_ELEMENTS } from 'src/app/core/core.module';
import { ResourceApi } from 'src/app/core/services/resource-api';
import * as fromApp from '../../store/app.reducer';
import * as EventActions from '../../events/store/events.actions';
import { Event } from 'src/app/shared/models/event';
import { map, take, tap } from 'rxjs/operators';
import { SegmentService } from 'ngx-segment-analytics';

const TOPIC_TYPES = [ 'Conference', 'Pitching Event', 'Trade Show', 'Virtual Event', 'Exhibition', 'Roundtables' ]

const INDUTRIES = [ 'FinTech', 'InsurTech', 'Financial Services', 'Tech' ]

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss']
})
export class IndexComponent implements OnInit {
  routeAnimationsElements = ROUTE_ANIMATIONS_ELEMENTS;
  topics = TOPIC_TYPES
  industries = INDUTRIES
  events: Event[] = []
  toShow: Event[] = []
  searchedTerm: string

  constructor(
    private resourceApi: ResourceApi,
    private store: Store<fromApp.AppState>,
    private segment: SegmentService) { }

  ngOnInit() {
    this.segment.track('Loaded Landing Page');
    this.store.dispatch(new EventActions.FetchEventsStart())

    this.store.select('eventsState')
              .pipe(map(eventState => { return eventState.events }))
              .subscribe(events => {
                this.events = events     
                this.toShow = this.events         
              })
  }

  onTopicClick(topic){
    this.searchedTerm = topic
    this.toShow = this.events

    let formattedTopic = topic.replace(/ /g, '').toLowerCase() //remove space and turn lowercase

    let toShow = []
    
    this.events.forEach((element, index) => {
      let features = element.features.split(',')
      let industry = element.industry.split(',')
      let topics = element.topics.split(',')

      let tags = features.concat(industry).concat(topics)
      let cleanTags = tags.map(tag => { return (tag.replace(/ /g, '')).toLowerCase()})

      if (cleanTags.includes(formattedTopic)){
        toShow.push(element)
      }
    });

    this.toShow = this.toShow.filter(event => toShow.includes(event))
  }

}
