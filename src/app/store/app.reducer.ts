import * as fromEvents from '../events/store/events.reducer'
import * as fromAuthentication from '../core/authentication/store/auth.reducer'
import * as fromProfile from '../account/profile/store/profile.reducer'
import { ActionReducerMap } from '@ngrx/store'

export interface AppState {
    eventsState: fromEvents.State;
    auth: fromAuthentication.State
    profile: fromProfile.State;
}

export const appReducer: ActionReducerMap<AppState> = {
    eventsState: fromEvents.EventsReducer,
    auth: fromAuthentication.authReducer,
    profile: fromProfile.profileReducer
}