import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';
import * as fromApp from '../../store/app.reducer'
import { map, take } from 'rxjs/operators';

@Injectable()
export class AppAuthGuard implements CanActivate{

    constructor(private authService: AuthService, 
                private router: Router,
                private store: Store<fromApp.AppState>) {
        
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot)
            : boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
        return this.store.select('auth')
                         .pipe(take(1), 
                               map((authState) => { return authState.user }),
                               map((user) => {
                                   if (user){
                                       const isAuth = !!user
                                       return isAuth
                                   }
                                   else {
                                        this.router.navigate(['/login'], { queryParams: { redirect: state.url }, replaceUrl: true})
                                   }
                               }))
        
        // if (this.authService.isAuthenticated()){
        //     return true
        // }

        // this.router.navigate(['/login'], { queryParams: { redirect: state.url }, replaceUrl: true})
        // return false
    }

}