import { Action } from '@ngrx/store'
import { User } from 'oidc-client'
import { UserRegistration } from 'src/app/shared/models/user.registration'

export const AUTHENTICATION_SUCCESS = '[Auth ] Login successfully'
export const LOGIN_AUTO = '[Auth ] ATUO LOGIN'
export const LOGOUT = '[Auth] LOGOUT'
export const SIGNUP_START = '[Auth] Sign Up Start'
export const SIGNUP_FAIL = '[Auth] Sign Up Fail'
export const SIGNUP_SUCCESS = '[Auth] Sign Up success'

export class AuthenticateSuccess implements Action {
    readonly type = AUTHENTICATION_SUCCESS

    constructor(public payload: User){}
}

export class LoginAuto implements Action {
    readonly type = LOGIN_AUTO
}

export class Logout implements Action {
    readonly type = LOGOUT
}

export class SignUpStart implements Action {
    readonly type = SIGNUP_START  
    constructor(public payload: UserRegistration){}
}

export class SignUpSuccess implements Action {
    readonly type = SIGNUP_SUCCESS
}

export class SignUpFail implements Action {
    readonly type = SIGNUP_FAIL

    constructor(public payload: string){}
}

export type AuthActions = AuthenticateSuccess 
                          | Logout 
                          | SignUpStart 
                          | SignUpSuccess 
                          | SignUpFail 
                          | LoginAuto