import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { User } from 'oidc-client';
import { of } from 'rxjs';
import { catchError, map, switchMap, tap } from 'rxjs/operators';
import { AuthService } from '../auth.service';
import * as AuthActions from './auth.actions'; 

@Injectable()
export class AuthEffects {
    handleAuthentication = (user: User) => {
        localStorage.setItem('userData', JSON.stringify(user))
        // this.authService.setLogoutTimer(+user.expires_in * 1000)        
        // this.authService.setLogoutTimer(3 * 1000)        
    }

    handleError = () => {

    }

    @Effect({dispatch: true})
    authActions = this.actions$
                      .pipe(ofType(AuthActions.SIGNUP_START), 
                            switchMap((action: AuthActions.SignUpStart) => {
                                // console.log('signup start', action.payload)
                                return this.authService
                                           .register(action.payload)
                                           .pipe(map(resData => {
                                                    return new AuthActions.SignUpSuccess()
                                                 }), 
                                                 catchError(errorRes => { 
                                                     const errorMsg = 'An unknown error occurred signing up'
                                                     return of(new AuthActions.SignUpFail(errorMsg))
                                                 }))
                            }))
    
    @Effect({dispatch: false})                        
    authLogin = this.actions$.pipe(ofType(AuthActions.AUTHENTICATION_SUCCESS), 
                                   tap((action: AuthActions.AuthenticateSuccess) => {
                                        this.handleAuthentication(action.payload)
                                   }))

    @Effect({dispatch: false})                               
    authLogout = this.actions$.pipe(ofType(AuthActions.LOGOUT), 
                                    tap(() => {
                                        localStorage.setItem('userData', null);
                                        this.authService.clearLogoutTimer();
                                        this.router.navigate(['/home'])
                                    }))

    @Effect() 
    authAutoLogin = this.actions$.pipe(ofType(AuthActions.LOGIN_AUTO),
                                       map(() => {
                                        const userData: User = JSON.parse(localStorage.getItem('userData'))
                                        if (!userData){
                                            return { type: 'NO_ACTION' }
                                        }
                                        if (userData && !userData.expired){
                                            // calculate the log out timer - 
                                            return new AuthActions.AuthenticateSuccess(userData)
                                        }
                                            
                                        return { type: 'NO_ACTION' }
                                       }))                               
    // @Effect({dispatch: false})                            
    // authRedirect = this.actions$.pipe(ofType(AuthActions.SIGNUP_SUCCESS, AuthActions.LOGOUT),
    //                                   tap(() => {
    //                                       this.router.navigate([])
    //                                   }))

    constructor(
        private actions$: Actions, 
        private authService: AuthService,
        private router: Router){}
}