import { Action } from '@ngrx/store';
import { User } from 'oidc-client';
import * as AuthActions from '../store/auth.actions'

export interface State {
    user: User,
    signingUp: boolean,
    signUpSuccess: boolean,
    signUpError: string
}

const initiateState: State = {
    user: null,
    signingUp: false,
    signUpSuccess: false,
    signUpError: null
}

export function authReducer(state = initiateState, action: AuthActions.AuthActions){
    switch (action.type){
        case AuthActions.SIGNUP_START:
            return {
                ...state,
                // user: null,                
                signingUp: true,
            }
        case AuthActions.SIGNUP_FAIL:
            return {
                ...state,
                signingUp: false,
                signUpError: action.payload
            }
        case AuthActions.SIGNUP_SUCCESS:
            return {
                ...state,
                signingUp: false,
                signUpSuccess: true
            }
        case AuthActions.AUTHENTICATION_SUCCESS:
            const user: User = action.payload
            return {
                ...state,
                user: user,
                signingUp: false,
                signUpSuccess: null,
                signUpError: null
            }
        case AuthActions.LOGOUT:
            return {
                ...state,
                user: null
            }
        default: 
            return state
    }
}