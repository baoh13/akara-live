import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';
import * as fromApp from '../../store/app.reducer';
import { AuthService } from './auth.service';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private router: Router, 
              private authService: AuthService,
              private store: Store<fromApp.AppState>) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot)
      : boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
    return this.store.select('auth')
                     .pipe(take(1),
                           map((authState) => { return authState.user}),
                           map((user) => {
                             const isAuth = !!user
                             if (isAuth){
                               return true
                             }
                             else {
                              this.router.navigate(['/login'], { queryParams: { redirect: state.url}, replaceUrl: true})
                              // return false
                             }
                           }))

    // if (this.authService.isAuthenticated()) { return true; }
    // this.router.navigate(['/login'], { queryParams: { redirect: state.url }, replaceUrl: true });
    // return false;
  }

}