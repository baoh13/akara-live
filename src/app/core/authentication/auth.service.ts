import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError, map, take } from 'rxjs/operators';
import { UserManager, UserManagerSettings, User } from 'oidc-client';
import { BehaviorSubject, Observable } from 'rxjs'; 

import { BaseService } from "../../shared/base.service";
import { ConfigService } from '../../shared/config.service';
import { Profile } from 'src/app/shared/models/profile';
import { Store } from '@ngrx/store';
import * as fromApp from '../../store/app.reducer'
import * as AuthActions from './store/auth.actions'

@Injectable({
  providedIn: 'root'
})
export class AuthService extends BaseService  {
  private manager = new UserManager(getClientSettings());
  private user: User | null;
  private profile: Profile;
  userObs: Observable<{user: User}>
  private tokenExpirationTimer: any;

  constructor(
    private http: HttpClient, 
    private configService: ConfigService,
    private store: Store<fromApp.AppState>) { 
    super();     
    
    // this.manager.getUser().then(user => { 
    //   this.user = user;
    //   // this._authNavStatusSource.next(this.isAuthenticated());
    // });

    this.userObs = this.store.select('auth')
    this.userObs.subscribe(authState => {
      this.user = authState.user
    })
  }

  // Auto login calculate expirationDate
  // user.expiredAt * 1000 in milliseconds
  // const expirationDuration = new Date(user.tokenExpirationDate).getTime() - new Date().getTime()

  setLogoutTimer(expirationDuration: number){
    this.tokenExpirationTimer = setTimeout(() => {
      this.store.dispatch(new AuthActions.Logout())
    }, expirationDuration)
  }

  clearLogoutTimer() {
    if (this.tokenExpirationTimer){
      clearTimeout(this.tokenExpirationTimer)
      this.tokenExpirationTimer = null
    }
  }

  login() { 
    return this.manager.signinRedirect();
  }

  async completeAuthentication() {
      this.user = await this.manager.signinRedirectCallback();
      
      this.store.dispatch(new AuthActions.AuthenticateSuccess(this.user))
      
      // console.log('complete auth user', this.user)
      if (this.user.profile){
        this.profile = {
          id: this.user.profile.sub,
          about: this.user.profile.About,
          email: this.user.profile.email,
          firstName: this.user.profile.name,
          surname: this.user.profile.family_name,
          jobTitle: this.user.profile.JobTitle,
          organization: this.user.profile.Organization,
          phoneNumber: this.user.profile.PhoneNumber,
          website: this.user.profile.website,
          name: this.user.profile.name
        }
      }   
  }  

  register(userRegistration: any) {    
    return this.http.post(this.configService.authApiURI + '/account', userRegistration)
                    .pipe(catchError(this.handleError));
  }

  update(profile: Profile){
    let url = this.configService.authApiURI + '/account';
    return this.http
               .patch(url, profile)
               .pipe(catchError(this.handleError))
  }

  async isAuthenticated() {    
    return this.user != null && !this.user.expired;
  }

  get personalProfile(): Profile {
    return this.profile
  }

  get userId(): string {    
    return this.user.profile.sub
  }

  getAuthorizationHeaderValue(user: User): string {
    return `${user.token_type} ${user.access_token}`;
  }

  getName(user: User){
    return user != null ? user.profile.name : '';
  }

  get name(): string {
    return this.user != null ? this.user.profile.name : '';
  }

  async signout() {
    this.store.dispatch(new AuthActions.Logout())
    await this.manager.signoutRedirect();
  }
}

export function getClientSettings(): UserManagerSettings {
  return {
      client_id: 'angular_spa',
      // redirect_uri: 'http://localhost:4200/auth-callback',
      // post_logout_redirect_uri: 'http://localhost:4200/',
      // silent_redirect_uri: 'http://localhost:4200/silent-refresh.html',
      // authority: 'https://localhost:32802',
      authority: 'https://akara-auth-server.azurewebsites.net',
      redirect_uri: 'https://www.akaralive.com/auth-callback',
      post_logout_redirect_uri: 'https://www.akaralive.com/',
      silent_redirect_uri: 'https://www.akaralive.com/silent-refresh.html',
      response_type:"id_token token",
      scope:"openid profile email api.read",
      filterProtocolClaims: true,
      loadUserInfo: true,
      automaticSilentRenew: true,
      metadata: {
        issuer: 'https://akara-auth-server.azurewebsites.net',
        jwks_uri: 'https://akara-auth-server.azurewebsites.net/.well-known/openid-configuration/jwks',
        end_session_endpoint: 'https://akara-auth-server.azurewebsites.net/connect/endsession',
        authorization_endpoint: 'https://akara-auth-server.azurewebsites.net/connect/authorize',
        userinfo_endpoint: 'https://akara-auth-server.azurewebsites.net/connect/userinfo',
      }, 
  };
}
