import { NgModule, Optional, SkipSelf } from '@angular/core'; 
import { AuthService } from './authentication/auth.service';
import { ResourceApi } from './services/resource-api';
import { AuthGuard } from './authentication/auth.guard';
import { AppAuthGuard } from './authentication/app.auth.guard';
import { routeAnimations, ROUTE_ANIMATIONS_ELEMENTS } from './animations/route.animations';
 

export {
  routeAnimations,
  ROUTE_ANIMATIONS_ELEMENTS
}

@NgModule({
  imports: [],
  exports: [],
  providers: [
    AuthService,
    ResourceApi,
    AuthGuard,
    AppAuthGuard
  ]
})
export class CoreModule {

  constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
    // Import guard
    if (parentModule) {
      throw new Error(`${parentModule} has already been loaded. Import Core module in the AppModule only.`);
    }
  }

}