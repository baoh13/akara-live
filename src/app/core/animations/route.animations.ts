import { animate, query, stagger, style, transition, trigger } from '@angular/animations'

export const ROUTE_ANIMATIONS_ELEMENTS = "route-animations-elements"

const STEP_ALLS: any[] = [
    query(':enter .'+ ROUTE_ANIMATIONS_ELEMENTS, 
         style({ opacity: 0 }), 
         { optional: true }),
    query(':enter .' + ROUTE_ANIMATIONS_ELEMENTS, 
          stagger(75, [
              style({ opacity: 0, transform: 'translateY(10%)' }),
              animate('2s ease-in-out', 
                      style({ opacity: 1, transform: 'translateY(0%)' }))
          ]),
          { optional: true })
]

const STEPS_ELEMENTS = [STEP_ALLS[0], STEP_ALLS[1]]

export const routeAnimations = trigger('routeAnimations', [
    transition(runAnimation, STEPS_ELEMENTS)
])

export function runAnimation() {
    return true
}