import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from "@angular/core";
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';
import { Review } from 'src/app/shared/models/review';
import { Event } from 'src/app/shared/models/event';
import { tap } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Injectable()
export class ResourceApi{
    readonly BASE_URL = 'https://akara-resource-api.azurewebsites.net/api'
    // readonly BASE_URL = 'http://localhost:32796/api'

    private _eventsSource = new BehaviorSubject<Event[]>([]);
    events$ = this._eventsSource.asObservable();
    
    reviews = new Subject<Review[]>()

    constructor(private httpClient: HttpClient) {
    }

    values(){
        let url = this.BASE_URL + '/values'
        
        return this.httpClient.get(url).subscribe(res => {
            // console.log(res)
        }, error => {
            // console.log(error)
        })
    }

    addReview(review: Review){
        let url = this.BASE_URL+ '/reviews'
        let body = review

        return this.httpClient.post(url, body);
    }

    getReviewsByEvent(eventId){
        let url = this.BASE_URL+ '/reviews'

        let params = new HttpParams().set('eventId', eventId)

        return this.httpClient.get<Review[]>(url, {params: params})
                              .pipe(tap(res => {
                                    this.reviews.next(res)
        }));
    }

    updateReview(review: Review){
        let url = this.BASE_URL+ '/reviews'
        let body = review

        return this.httpClient.patch(url, body);
    }

    getEvents(){
        const url = this.BASE_URL + '/events'

        return this.httpClient.get<Event[]>(url)
    }
}