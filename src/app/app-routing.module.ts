import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthCallbackComponent } from './auth-callback/auth-callback.component';
 

const routes: Routes = [
  { path: 'auth-callback', component: AuthCallbackComponent },
  // Fallback when no prior route is matched - wont work for links with query params
  // { path: '**', redirectTo: '', pathMatch: 'full' }
];

@NgModule({
  // useHash eliminates "Cannot retrieve the URL specified in the Content Link property" error in SharePoint
  // imports: [RouterModule.forRoot(routes, { useHash: true })],
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { 

  constructor() {
    
  }
}
