import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthService } from '../../core/authentication/auth.service';
import { Observable, Subscription } from 'rxjs';
import { Store } from '@ngrx/store';
import * as fromApp from '../../store/app.reducer'
import { map, take } from 'rxjs/operators';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnDestroy {

  name: string;
  isAuthenticated: boolean;
  subscription:Subscription;

  constructor(private authService:AuthService, private store: Store<fromApp.AppState>) { }

  async ngOnInit() {
    this.subscription = this.store.select('auth').pipe(take(1), map(authState => { return authState.user}))
                                        .subscribe(user => {
                                          this.isAuthenticated = !!user;
                                          if (this.isAuthenticated){
                                            this.name = this.authService.getName(user)
                                          }
                                        });
  } 

   async signout() {
    await this.authService.signout();     
  }

  ngOnDestroy() {
    // prevent memory leak when component is destroyed
    this.subscription.unsubscribe();
  }
}
