import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { map } from 'rxjs/operators';
import { AuthService } from 'src/app/core/authentication/auth.service';
import * as fromApp from '../../store/app.reducer'

@Component({
  selector: 'app-menu-icon',
  templateUrl: './menu-icon.component.html',
  styleUrls: ['./menu-icon.component.scss']
})
export class MenuIconComponent implements OnInit {
  userName = ''
  constructor(
    private authService: AuthService, 
    private store: Store<fromApp.AppState>) { }

  ngOnInit() {
    this.store.select('auth')
              .pipe(map(authState => {
                return authState.user
              }))
              .subscribe(user => {
                 if (user && user.profile){
                  this.userName = user.profile.name
                 } 
              })
  }

  async onLogOut(){
    await this.authService.signout();
  }
}
