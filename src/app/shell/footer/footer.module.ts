import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { SharedModule } from 'src/app/shared/shared.module';
import { FooterComponent } from './footer.component';

@NgModule({
    imports: [
        RouterModule,
        CommonModule,
        FontAwesomeModule,
        SharedModule
    ],
    declarations: [FooterComponent],
    exports: [
        FooterComponent
    ]
})
export class FooterModule {

}