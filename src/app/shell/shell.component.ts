import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { faCoffee } from '@fortawesome/free-solid-svg-icons'
import * as fromApp from '../store/app.reducer';
import * as AuthActions from '../core/authentication/store/auth.actions'
import { map, take } from 'rxjs/operators';
import { routeAnimations } from '../core/core.module';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../core/authentication/auth.service';

@Component({
  selector: 'app-shell',
  templateUrl: './shell.component.html',
  styleUrls: ['./shell.component.scss'],
  animations: [routeAnimations]
})
export class ShellComponent implements OnInit {
  isReviewForm = false
  navigation = [
    // { link: '/evaluation-form', label: 'Evaluation Form'},
    // { link: '', label: 'Add Events'},
    // { link: '', label: 'Add Review'},
    // { link: '', label: 'For Event Organizers'}
  ]

  unauthenticatedNavigationMenu = [
    ...this.navigation,
    { link: '/register', label: 'Sign Up'},
    { link: '/login', label: 'Login'},
  ]

  authenticatedNavigationMenu = [
    ...this.navigation,
    // { link: '/topsecret', label: 'Top Secret'}
    { link: '/profile', label: 'Profile'}
  ]

  isAuthenticated: boolean = false

  // logo = require("../../assets/images/angular_solidBlack.svg").default
  // logo = require('../../assets/images/angular_solidBlack.svg').default


  constructor(
    private authService: AuthService,
    private store: Store<fromApp.AppState>, 
    private route: ActivatedRoute) {}

  ngOnInit() {
    this.store.select('auth')
              .pipe(take(1), 
                    map(authState => { return authState.user}))
              .subscribe(user => {
                if (user){
                  this.isAuthenticated = true
                }
              })
    
    // FirstChild of Shell tex: /review-form
    this.route.firstChild.url.subscribe(url => {
      if(url[0].path.includes('review-form')){
        // console.log('url', url[0])
        this.isReviewForm = true
      }
    });
  }

  async onLogOut(){
    await this.authService.signout();
  }
}
