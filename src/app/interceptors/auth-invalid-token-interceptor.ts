import { HttpErrorResponse, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Store } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import * as fromApp from '../store/app.reducer'
import * as AuthActions from '../core/authentication/store/auth.actions'
import { Router } from '@angular/router';
import { Injectable } from '@angular/core';

@Injectable()
export class AuthInvalidTokenInterceptor implements HttpInterceptor {

    constructor(private store: Store<fromApp.AppState>, private router: Router){}

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<any> {                
        return next.handle(req)
                   .pipe(catchError((err: HttpErrorResponse) => {
                            console.log('response interceptor', err)                          
                            const errorCode = err.status
                            // const authHeader = err.headers['WWW-Authenticate']
                            // console.log('headers', err.headers)
                            if (errorCode === 401){ //Unauthorized // Todo check invalid token   
                                // if (authHeader && authHeader.includes('The token expired at')){
                                // if (authHeader && authHeader.includes('invalid_token')){
                                    // console.log('invalid token interceptor', err)
                                // this.store.dispatch(new AuthActions.Logout())                                    
                                // }   
                                // this.router.navigate(['/home'])                             
                            }
                        return of()
                    }));
    }

}