import { Injectable } from "@angular/core";
import { Observable } from 'rxjs';
import { HttpEvent, HttpRequest, HttpHandler } from '@angular/common/http';

@Injectable()
export class HttpsInterceptor implements HttpsInterceptor{
    constructor(){}

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>>{
        // console.log('request url', req.url)
        if (req.url.startsWith('http://')){
            console.log('req', req)
            const httpsReq = req.clone({
                url: req.url.replace('http://', 'https://')
            })
            console.log('httpsReq', httpsReq)
            return next.handle(httpsReq)
        }

        return next.handle(req)
    }
}