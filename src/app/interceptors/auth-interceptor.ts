import { HttpEvent, HttpHandler, HttpHeaders, HttpInterceptor, HttpParams, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { User } from 'oidc-client';
import { Observable, of } from 'rxjs';
import { exhaustMap, map, take } from 'rxjs/operators';
import { AuthService } from '../core/authentication/auth.service';
import * as fromApp from '../store/app.reducer'
import * as AuthActions from '../core/authentication/store/auth.actions'
import { Router } from '@angular/router';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

    constructor(private authService: AuthService, 
                private store: Store<fromApp.AppState>,
                private router: Router) {}

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<any> {

        return this.store.select('auth')
                   .pipe(take(1),
                         map((authState) => { return authState.user }),
                         exhaustMap((user: User) => {
                                if (user){
                                    // console.log('User', user)                                    
                                    const isExpired =  new Date(user.expires_at * 1000).getTime() < new Date().getTime() 
                                    const expirationDuration = new Date(user.expires_at * 1000).getTime() - new Date().getTime()                                  
                                    // console.log('Expired in ', (expirationDuration))
                                    if (isExpired){ // token expired -
                                        this.store.dispatch(new AuthActions.Logout())
                                        this.router.navigate(['/login'])
                                        return of()
                                    }
                                    else {
                                        const authReq = req.clone({
                                            headers: new HttpHeaders({
                                                    'Content-Type': 'application/json',
                                                    'Authorization': this.authService.getAuthorizationHeaderValue(user)
                                            })})
                                        return next.handle(authReq)
                                    }                                    
                                }
                                else {
                                    return next.handle(req)
                                }}))

        // if (this.authService.isAuthenticated()){
        //     let authenticatedReq = req.clone(
        //         { 
        //             // params: new HttpParams().set('Authorization', this.authService.authorizationHeaderValue), 
        //             headers: new HttpHeaders({
        //                 'Content-Type': 'application/json',
        //                 'Authorization': this.authService.authorizationHeaderValue
        //             })
        //         });
            
        //     return next.handle(authenticatedReq)
        // }

        // return next.handle(req);
    }

}