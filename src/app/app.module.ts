import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

// used to create fake backend
import { FakeBackendProvider } from './shared/mocks/fake-backend-interceptor';
import { HttpsInterceptor } from './interceptors/https-interceptor';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ConfigService } from './shared/config.service';
import { AuthCallbackComponent } from './auth-callback/auth-callback.component';


/* Module Imports */
import { CoreModule } from './core/core.module';
import { HomeModule }  from './home/home.module';
import { AccountModule }  from './account/account.module';
import { ShellModule } from './shell/shell.module';
import { TopSecretModule } from './top-secret/top-secret.module';
import { SharedModule }   from './shared/shared.module';
import { EvaluationFormModule } from './evaluation-form/evaluation-form.module';
import { EventsModule } from './events/events-module';
import { AuthInterceptor } from './interceptors/auth-interceptor';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { StoreModule } from '@ngrx/store'
import { SegmentModule } from 'ngx-segment-analytics';
import * as fromApp from '../app/store/app.reducer'
import { EffectsModule } from '@ngrx/effects'
import { EventsEffects } from './events/store/events.effects';
import { AuthEffects } from './core/authentication/store/auth.effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools'
import { StoreRouterConnectingModule } from '@ngrx/router-store'
import { ProfileEffects } from './account/profile/store/profile.effects';
import { FaIconLibrary} from '@fortawesome/angular-fontawesome'
import {
  faCog,
  faBars,
  faRocket,
  faPowerOff,
  faUserCircle,
  faPlayCircle
} from '@fortawesome/free-solid-svg-icons';
import {
  faGithub,
  faMediumM,
  faTwitter,
  faInstagram,
  faYoutube
} from '@fortawesome/free-brands-svg-icons';
import { AuthInvalidTokenInterceptor } from './interceptors/auth-invalid-token-interceptor';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ReviewFormModule } from './review-form/review-form.module';
import { ReviewFormDialogComponent } from './review-form/review-form-dialog.component';

@NgModule({
  declarations: [
    AppComponent,
    AuthCallbackComponent,
    ReviewFormDialogComponent
  ],
  imports: [
    BrowserModule,  
    HttpClientModule, 
    CoreModule,
    HomeModule,
    AccountModule,
    TopSecretModule,   
    AppRoutingModule,
    ShellModule,   
    SharedModule,
    EvaluationFormModule,
    EventsModule,
    ReviewFormModule,
    SegmentModule.forRoot({ apiKey: 'VNmGh8w6p0IxFaZUkKr2XkmCJ9ixLcfb', debug: true, loadOnInitialization: true }),
    FlexLayoutModule,
    StoreModule.forRoot(fromApp.appReducer),
    EffectsModule.forRoot([EventsEffects, AuthEffects, ProfileEffects]),
    BrowserAnimationsModule, // adding angular mat
    StoreDevtoolsModule.instrument({
      maxAge: 25,
      // logOnly: environment.production, // Restrict extension to log-only mode
    }),
    StoreRouterConnectingModule.forRoot()
  ],
  providers: [
    ConfigService,
    // provider used to create fake backend
    FakeBackendProvider,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInvalidTokenInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpsInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent],
  entryComponents: [ReviewFormDialogComponent]
})
export class AppModule { 
  constructor(faIconLibrary: FaIconLibrary){
    faIconLibrary.addIcons(
      faCog,
      faBars,
      faRocket,
      faPowerOff,
      faUserCircle,
      faPlayCircle,
      faGithub,
      faMediumM,
      faTwitter,
      faInstagram,
      faYoutube
    )
  }
}
