# Stage 0, "build-stage", based on Node.js, to build and compile the frontend
FROM tiangolo/node-frontend:10 as build-stage

RUN mkdir -p /app
WORKDIR /app
COPY package.json /app
RUN npm install
COPY ./ /app/

ARG configuration=production
RUN npm run build -- --output-path=./dist/oauth-client --configuration $configuration

# Stage 1, based on Nginx, have only the compiled app and ready for prod with Nginx
FROM nginx:1.17.1-alpine
COPY --from=build-stage /app/dist/oauth-client /usr/share/nginx/html

# Copy the default nginx.conf provided by tiangolo/node-frontend
COPY --from=build-stage /nginx.conf /etc/nginx/conf.d/default.conf

# docker build -t akara-live .
# docker run -d -it -p 8080:80/tcp --name akara-live akara-live:latest
# docker tag asdasd baoh13/akara-live:latest
# docker push baoh13/akara-live


#git remote add origin https://baoh13@bitbucket.org/baoh13/akara-live.git
# git push -u origin master
# git remote -v


# git remote add origin https://github.com/baoh13/1.git
# git branch -M main
# git push -u origin main 